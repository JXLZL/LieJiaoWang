--添加供应产品分类表
CREATE TABLE t_sell_type (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`name` VARCHAR ( 200 ) NOT NULL COMMENT '名称',
`sort_index` int NOT NULL COMMENT '排序',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '供应产品表';

--删除供应产品原有分类字段
alter table t_product_sell drop column type;
--add new type property
alter table t_product_sell add column type_id varchar(32) not null comment '分类id';
alter table t_product_sell add column home_page bit default 0 comment '首页显示：0不显示 1显示';
alter table t_product_sell add column home_page_index int default 0 comment '首页显示下标';