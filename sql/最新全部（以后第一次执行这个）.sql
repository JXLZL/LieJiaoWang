CREATE DATABASE yw_liejiao_web_develop DEFAULT CHARACTER
SET utf8 COLLATE utf8_unicode_ci;
USE yw_liejiao_web_develop;

CREATE TABLE t_company (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`name` VARCHAR ( 300 ) NOT NULL COMMENT '名称',
`area` VARCHAR ( 200 ) NOT NULL COMMENT '地区',
`contact` VARCHAR ( 100 ) NOT NULL COMMENT '联系人',
`email` VARCHAR ( 30 ) NOT NULL COMMENT '邮箱',
`zip` VARCHAR ( 10 ) NOT NULL COMMENT '邮政编码',
`phone` VARCHAR ( 20 ) NOT NULL COMMENT '电话',
`intro` VARCHAR ( 900 ) NOT NULL COMMENT '简介',
`detail` text NOT NULL COMMENT '详细资料',
`image_url` VARCHAR ( 200 ) NOT NULL COMMENT '图片url',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '公司表';

CREATE TABLE t_image (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`name` VARCHAR ( 300 ) NOT NULL COMMENT '图片名称',
`url` VARCHAR ( 200 ) NOT NULL COMMENT '图片URL',
`real_path` VARCHAR ( 300 ) NOT NULL COMMENT '物理存储路径',
`module` VARCHAR ( 30 ) DEFAULT NULL COMMENT '所属模块',
`ref_count` int NOT NULL DEFAULT 1 COMMENT '引用次数',
`collected` bit ( 1 ) NOT NULL DEFAULT 0 COMMENT '是否收藏：0否 1是',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '图片表';

CREATE TABLE t_news (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`title` VARCHAR ( 300 ) NOT NULL COMMENT '标题',
`author` VARCHAR ( 300 ) DEFAULT NULL COMMENT '作者',
`source` VARCHAR ( 300 ) DEFAULT NULL COMMENT '来源',
`intro` VARCHAR ( 900 ) DEFAULT NULL COMMENT '简介',
`image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '图片url',
`content` TEXT DEFAULT NULL COMMENT '内容',
`comment` VARCHAR ( 900 ) DEFAULT NULL COMMENT '备注',
`visit` int NOT NULL DEFAULT 0 COMMENT '阅读次数',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '新闻表';

CREATE TABLE t_seller (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`company_name` VARCHAR ( 100 ) NOT NULL COMMENT '公司名称',
`contact` VARCHAR ( 30 ) NOT NULL COMMENT '联系人',
`email` VARCHAR ( 30 ) NOT NULL COMMENT '邮箱',
`phone` VARCHAR ( 11 ) NOT NULL COMMENT '手机',
`area` VARCHAR ( 30 ) NOT NULL COMMENT '地区',
`audited` bit NOT NULL COMMENT '是否已经审核：0否 1是',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '卖家表';

CREATE TABLE t_buyer (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`company_name` VARCHAR ( 100 ) NOT NULL COMMENT '公司名称',
`contact` VARCHAR ( 30 ) NOT NULL COMMENT '联系人',
`email` VARCHAR ( 30 ) NOT NULL COMMENT '邮箱',
`phone` VARCHAR ( 11 ) NOT NULL COMMENT '手机',
`area` VARCHAR ( 30 ) NOT NULL COMMENT '地区',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '买家表';


CREATE TABLE t_sell_type (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`name` VARCHAR ( 200 ) NOT NULL COMMENT '名称',
`sort_index` int NOT NULL COMMENT '排序',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '供应产品表';

CREATE TABLE t_product_sell (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`type_id` VARCHAR ( 32 ) DEFAULT NULL COMMENT '卖家id',
`seller_id` VARCHAR ( 32 ) DEFAULT NULL COMMENT '卖家id',
`name` VARCHAR ( 200 ) NOT NULL COMMENT '名称',
`key` VARCHAR ( 200 ) DEFAULT NULL COMMENT '关键词',
`description` VARCHAR ( 3000 ) DEFAULT NULL COMMENT '描述',
`content` text DEFAULT NULL COMMENT '内容',
`price` VARCHAR ( 300 ) DEFAULT NULL COMMENT '价格',
`phone` VARCHAR ( 11 ) DEFAULT NULL COMMENT '手机',
`qq` VARCHAR ( 12 ) DEFAULT NULL COMMENT 'qq',
`image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '图片url',
`visits` int DEFAULT 0 COMMENT '浏览次数',
`home_page` bit default 0 COMMENT '首页显示：0不显示 1显示',
`home_page_index` int default 0 COMMENT '首页显示下标',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '供应产品表';

CREATE TABLE t_product_buy (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`buyer_id` VARCHAR ( 32 ) DEFAULT NULL COMMENT '买家id',
`name` VARCHAR ( 200 ) NOT NULL COMMENT '名称',
`key` VARCHAR ( 200 ) DEFAULT NULL COMMENT '关键词',
`description` VARCHAR ( 3000 ) DEFAULT NULL COMMENT '描述',
`content` text DEFAULT NULL COMMENT '内容',
`price` VARCHAR ( 300 ) DEFAULT NULL COMMENT '价格要求',
`pack` VARCHAR ( 300 ) DEFAULT NULL COMMENT '包装要求',
`number` VARCHAR ( 100 ) DEFAULT NULL COMMENT '需求数量',
`phone` VARCHAR ( 11 ) DEFAULT NULL COMMENT '手机',
`qq` VARCHAR ( 12 ) DEFAULT NULL COMMENT 'qq',
`image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '图片url',
`visits` int DEFAULT 0 COMMENT '浏览次数',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '求购产品表';

CREATE TABLE t_user (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`username` VARCHAR ( 32 ) NOT NULL COMMENT '用户名',
`password` VARCHAR ( 36 ) NOT NULL COMMENT '密码',
`phone` VARCHAR ( 11 ) NOT NULL COMMENT '手机',
`email` VARCHAR ( 30 ) NOT NULL COMMENT '邮箱',
`company_name` VARCHAR ( 100 ) NOT NULL COMMENT '公司名称',
`ref_id` VARCHAR ( 32 ) DEFAULT NULL COMMENT '关联公司id',
`role` VARCHAR ( 30 ) NOT NULL COMMENT '角色：管理员 卖家 买家',
`last_login_ip` VARCHAR ( 30 ) DEFAULT NULL COMMENT '最后一次登录IP',
`last_login_time` datetime DEFAULT NULL COMMENT '最后一次登录时间',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '用户表';

CREATE TABLE t_banner (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`name` VARCHAR ( 32 ) DEFAULT NULL COMMENT '名称',
`image_url` VARCHAR ( 200 ) NOT NULL COMMENT '图片url',
`link_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '链接url',
`sort_index` int NOT NULL COMMENT '排序',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '滚图表';


-- 初始化卖家
INSERT INTO `t_seller`(`id`, `create_id`, `create_time`, `update_id`, `update_time`, `deleted`, `company_name`, `contact`, `email`, `phone`, `area`, `audited`) VALUES ('liejiao', 'system', '2018-06-06 18:06:31', 'system', '2018-06-06 18:06:39', b'0', '上海鑫冠橡胶制品有限公司', '汪加玉', 'wjv2009@126.com', '13621555132', '昆山市青阳支路99号', b'1');
-- 初始化公司
INSERT INTO `t_company`(`id`, `create_id`, `create_time`, `update_id`, `update_time`, `deleted`, `name`, `area`, `contact`, `email`, `zip`, `phone`, `intro`, `detail`, `image_url`) VALUES ('liejiao', 'system', '2018-06-09 13:44:53', 'system', '2018-06-09 13:44:55', b'0', '猎胶', '猎胶', '猎胶', '猎胶', '猎胶', '13111111111', '猎胶', '猎胶', '猎胶');

