$(function () {
    var id = getQueryString("info");
    $.ajax({
        url: host() + '/productSell/get/' + id,
        success: function (res) {
            var pdetail = res.data;
            console.log(pdetail);
            var imgurl = host() + pdetail.imageUrl;
            $('.img1').attr('src', imgurl);
            $('.pic img').attr('src', imgurl);
            $('.m-t-left h3').html(pdetail.name);
            $('.jiage i').html(pdetail.price);
            $('.miaoshu i').html(pdetail.description);
            $('.phone i').html(pdetail.phone);
            $('.qq i').html(pdetail.qq);
            $('.key i').html(pdetail.key);
            $('.time i').html(pdetail.createTime.substr('0', 10));
            $('.content').html(pdetail.content);
            $('.count i').html(pdetail.visits);

            //浏览次数添加
            $.ajax({
                url: host() + '/productSell/visit/' + id,
                type:'PUT',
                success: function (res) {
                    console.log(res);
                }
            });

            $.ajax({
                url: host() + '/seller/get/' + pdetail.sellerId,
                success: function (res) {
                    var comp = res.data;
                    $('.comp').html(comp.companyName);
                    $('.contact b').html(comp.contact);
                    $('.email b').html(comp.email);
                    $('.shouji b').html(comp.phone);
                    $('.area b').html(comp.area);
                }
            });

        }
    });
});