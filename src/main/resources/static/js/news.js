/*
var pagenum = Cookies.get('page');
if (pagenum == undefined) {
    pagenum = 1
}
Cookies.remove('page')

$.ajax({
    url: host() + '/news/pageQuery?pageNo=' + pagenum + '&pageSize=20'
}).done(function (e) {
    var newlist = e.data.rows;
    console.log(newlist)
    var list1 = '';

    for (var i = 0, len = newlist.length; i < len; i++) {
        // console.log(i)
        var d = new Date(newlist[i].createTime);
        var time = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
        list1 += '<li numId="' + i + '">' + newlist[i].title + '<span>' + time + '</span></li>';
    }
    $('.new-nav').html(list1);

    $('.new-nav li').each(function (d) {
        $(this).click(function () {
            // console.log(d)
            Cookies.set('newId', newlist[d].id, 100);
            window.location.href = 'newdetail.html';
        })
    })

    //原始
    // $('.new-nav li').each(function () {
    //     $(this).click(function () {
    //         n = $(this).attr('numId');
    //         console.log(newlist[n].id)
    //         Cookies.set('id',newlist[n],100)
    //         window.location.href='newdetail.html';
    //     })
    // })
    $('.page-link').eq(pagenum - 1).addClass('active')

    $.ajax({
        url: host() + '/news/pageQuery?pageNo=1&pageSize=999',
    }).done(function (d) {
        var p1 = '';
        var pagelist = d.data.rows;
        for (var j = 1, leng = Math.ceil(pagelist.length / 20); j <= leng; j++) {
            p1 += '<li class="page-item"><a class="page-link" num="' + j + '" href="javascript:;">' + j + '</a></li>'
        }
        $('.pagination').html(p1);
        $('.page-link').click(function () {
            var page = $(this).attr('num')
            Cookies.set('page', page, 100)
            window.location.reload();
        })
    })
});
*/

$(function () {
    $.ajax({
        url: host() + '/news/pageQuery?pageNo=1&pageSize=9999',
        success: function (res) {
            if (!res.data.rows) {
                $('.m-l-nav').html('<div class="no_pro">没有新闻！</div>');
                return false
            }
            //把allList赋值，所有的产品
            allList = res.data.rows;
            //第二部，渲染产品列表，初次渲染
            show(1);

            page();

        }
    });
});

//渲染产品界面函数
function show(e) {
    showList = [];
    //定义长度，换算
    var len = allList.length < e * 20 ? allList.length : e * 20;

    //使用for循环，元素添加到列表里面
    for (var s = (e - 1) * 20; s < len; s++) {
        showList.push(allList[s]);
    }

    //定义一个空的界面，渲染
    var html = '';
    for (var i in showList) {
        var d = new Date(showList[i].createTime);
        var time = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
        html += '<li num="' + showList[i].id + '">' + showList[i].title + '<span>' + time + '</span></li>';
    }
    $('.new-nav').html(html);

    //更新点击事件
    newClick();
}

//更新页码操作
function page() {
    var page = '';
    //页码数量，向上取整，每页是5个产品
    var len = Math.ceil(allList.length / 20);
    //第一步，先把页码渲染出来
    for (var i = 1; i <= len; i++) {
        page += '<li><a class="page-link" num="' + i + '">' + i + '</a></li>'
    }
    $('.pagination').html(page);

    //给每一个页码添加点击事件,num是当前页数
    var num = '';
    $('.page-link').click(function () {
        num = $(this).attr('num');
        show(num);
    })
}

//产品点击事件
function newClick() {
    $('.new-nav li').each(function () {
        $(this).click(function () {
            var info = $(this).attr('num');
            window.location.href = 'newdetail.html?info=' + info;
        });
    });
};
