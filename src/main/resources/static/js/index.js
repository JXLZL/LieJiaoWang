$(function () {
    //先把已登录隐藏，再根据cookie字段name存在判断，是则一登录显示，未登录隐藏，同时把账号，身份，最后一次登录时间显示
    var name = Cookies.get('name');
    if (name) {
        $('.login').css('display', 'block');
        $('.login-box').css('display', 'none');
        $('.u_name').text('账号：' + name);
        $('.role').text('身份：' + Cookies.get('role'));
        $('.time').text('最近一次登录时间：' + Cookies.get('time'));
    }

    //获取供应产品的列表，渲染
    getPro();

    //获取求购产品列表，渲染
    needPro();


    //获取新闻列表
    getNews();


    //获取轮播图
    getBanner();

    //企业展示
    showCompany();


    //广告的隐藏
    $('.gg em').click(function () {
        $(this).parents('.gg').hide();
    });

    //买家的点击选中时间
    $('.maijia').click(function () {
        $(this).addClass('active');
        $('.gys').removeClass('active');
    });
    //供应商的点击选中时间
    $('.gys').click(function () {
        $(this).addClass('active');
        $('.maijia').removeClass('active');
    });

    //去公司介绍
    $('.m-t-right p').click(function () {
        window.location.href = 'company.html'
    });

    //获取身份信息
    $('.fabu').click(function () {
        var role = Cookies.get('role');
        if (role == '买家') {
            window.location.href = 'manage/buyerbox1.html';
        } else if (role == '管理员') {
            window.location.href = 'manage/qiyeguanli.html';
        } else if(role == '卖家'){
            window.location.href = 'manage/sellerbox1.html';
        }else {
            alert('身份出错！');
        }
    });

    //退出点击事件
    logout();

    //登录事件
    login();

});

//获取产品列表
function getPro() {
    $.ajax({
        //url: host() + '/productSell/pageQuery?pageNo=1&pageSize=12',
        url: host() + '/productSell/homePage?size=12',
        success: function (e) {
            //var plist = e.data.rows;
            var plist = e.data;
            console.log(plist);
            var html = '';
            var nav1 = '';
            var len = plist.length < 12 ? plist.length : 12;
            if (!len) {
                $('.m-f-right').html('<div>没有供应产品！</div>');
                $('.m-nav1').html('<div>没有供应产品！</div>');
                return false
            }
            for (var i = 0; i < len; i++) {
                html += '<li class="clear" num="' + plist[i].id + '">\n' + '<img src="' + host() + '' + plist[i].imageUrl + '">\n' + '<div class="n-txt">\n' +
                    '<p>名称：' + plist[i].name + '</p>\n' + '<p>价格：' + plist[i].price + '</p>\n' + '<p>手机：' + plist[i].phone + '</p>\n' +
                    '<p>qq：' + plist[i].qq + '</p>\n' + '<p>供货公司：佛山德汇商贸有限公司</p>\n' + '<span>' + plist[i].createTime.substr('0', 10) + '\n' +
                    '<b></b>\n' + '<em></em>\n' + '</span>\n' + '</div>\n' + '<div class="btn">\n' + '<a class="mianyi">\n' + '面议\n' +
                    '</a>\n' + '<a class="xunjia">\n' + '询价\n' + '</a>\n' + '</div>\n' + '</li>';

                nav1 += '<li num="' + plist[i].id + '">\n' + '<i></i>\n' + plist[i].name + '\n' + '</li>';
            }
            $('.m-f-right').html(html);
            $('.m-nav1').html(nav1);

            //给每一个供应产品添加点击事件
            $('.m-f-right li').each(function () {
                $(this).click(function () {
                    var id = $(this).attr('num');
                    //console.log(id);
                    //Cookies.set('info', id, 100);
                    window.location.href = 'product-detail.html?info=' + id;
                })
            });

            //给每一个供应产品添加点击事件
            $('.m-nav1 li').each(function () {
                $(this).click(function () {
                    var id = $(this).attr('num');
                    //console.log(id);
                    //Cookies.set('info', id, 100);
                    window.location.href = 'product-detail.html?info=' + id;
                })
            })

        }
    });
};

//求购产品列表
function needPro() {
    $.ajax({
        url: host() + '/productBuy/pageQuery?pageNo=1&pageSize=8',
        success: function (res) {
            var list = res.data.rows;
            if (!res.data.rows.length) {
                $('.m-nav2').html('<div>没有求购产品！</div>');
                return false
            }
            var nav2 = '';
            for (var j = 0, len = list.length; j < len; j++) {
                nav2 += '<li num="' + list[j].id + '">\n' + '<i></i>\n' + list[j].name + '\n' + '</li>'
            }
            $('.m-nav2').html(nav2);
            //给每一个求购产品添加点击事件
            $('.m-nav2 li').each(function () {
                $(this).click(function () {
                    var id = $(this).attr('num');
                    //Cookies.set('info', id, 100);
                    window.location.href = 'list-detail.html?info=' + id;
                })
            })
        }
    });
};

//获取新闻列表
function getNews() {
    $.ajax({
        url: host() + '/news/pageQuery?pageNo=1&pageSize=4',
        success: function (res) {
            var list = res.data.rows;
            var nav4 = '';
            if (!list.length) {
                $('.m-nav4').html('<div>没有新闻！</div>');
                return false
            }
            for (var k = 0, len = list.length; k < len; k++) {
                nav4 += '<li class="clear" num="' + list[k].id + '">\n' + '<img src="' +
                    host() + '' + list[k].imageUrl + '">\n' + '<div class="nav-txt">\n' +
                    '<p>【' + list[k].title + '】</p>\n' + list[k].content + '\n' + '</div>\n' + '</li>';
            }
            $('.m-nav4').html(nav4);

            $('.m-nav4 li').each(function () {
                $(this).click(function () {
                    var id = $(this).attr('num');
                    Cookies.set('newId', id, 100);
                    window.location.href = 'newdetail.html?info=' + id;
                });
            })
        }
    });
};

//获取轮播图
function getBanner() {
    $.ajax({
        url: host() + '/banner/listAll',
        type: 'GET',
        success: function (res) {
            var swiper = res.data;
            if (!swiper.length) {
                $('.swiper-wrapper').html('<div>没有设置轮播图！</div>');
                return false
            }
            var nav4 = '';
            for (var k = 0, len = swiper.length; k < len; k++) {
                nav4 += '<div class="swiper-slide"><img src="' + host() + swiper[k].imageUrl + '"></div>';
            }
            $('.swiper-wrapper').html(nav4);
            new Swiper('.swiper-container', {
                loop: true,
                autoplay: true

            })
        }
    });
};

//登录事件
function login() {
    $('.denglu').click(function () {
        var formData = new FormData();
        var uname = $('.uname').val();
        var pass = $('.pass').val();
        formData.append('username', uname);
        formData.append('password', pass);
        $.ajax({
            url: host() + '/login',
            type: "POST",
            contentType: false,
            processData: false,
            async: false,
            dataType: "JSON",
            data: formData,
            success: function (e, s, k) {
                if(e.errors){
                    alert(e.errors[0].message);
                    return false
                }
                Cookies.set('name', e.data.companyName, 100);
                Cookies.set('refId', e.data.refId, 100);
                Cookies.set('userId', e.data.id, 100);
                Cookies.set('role', e.data.role, 100);
                var d = new Date();
                var time = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                Cookies.set('time', time, 100);
                //window.location.reload();
                switch (e.data.role) {
                    case '管理员':
                        window.location.href = './manage/qiyeguanli.html';
                        break;
                    case '买家':
                        $('.login-box').css('display', 'none');
                        $('.login').css('display', 'block');
                        $('.u_name').text('账号：' + e.data.companyName);
                        $('.role').text('身份：' + e.data.role);
                        $('.time').text('最近一次登录时间：' + time);
                        $('.weizhuce').hide();
                        $('.yizhuce .h6').html(e.data.companyName);
                        $('.yizhuce').show();
                        //window.location.href = './manage/buyerbox1.html';
                        window.location.reload();
                        break;
                    case '卖家':
                        window.location.href = './manage/sellerbox1.html';
                        break;
                    default :
                        alert("身份出错!");
                        break;
                }
            },
            error: function(res){
                alert("登录失败！");
            }
        })
    });
};

//退出登录事件
function logout() {
    $('.tuichu').click(function () {
        $.ajax({
            url: host() + '/logout',
            type: 'get',
            success: function (res) {
                if (res.data != 'logout success!') {
                    alert('退出登录失败');
                    return false
                }
                Cookies.remove('name');
                Cookies.remove('refId');
                Cookies.remove('role');
                Cookies.remove('time');
                window.location.reload();
            }
        });
    });
}

function showCompany() {
    $.ajax({
        url: host() + '/company/pageQuery?pageNo=1&pageSize=5',
        success: function (res) {
            var data = res.data.rows;
            var html = '';
            for(var i in data){
                html+='<li class="'+data[i].id+'"><b></b>'+data[i].name+'</li>';
            };
            $('.m-nav3').html(html);
            $('.m-nav3 li').each(function(){
                $(this).click(function(){
                    var id = $(this).attr('class');
                    window.location.href = 'qiyejieshao.html?info=' + id;
                })
            })
        }
    });
    //m-nav3
}