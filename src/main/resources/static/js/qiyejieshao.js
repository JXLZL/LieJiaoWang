$(function () {
    var id = getQueryString('info');
    $.ajax({
        url: host() + '/company/get/' + id,
        success: function (res) {
            var info = res.data;
            $('.gongsi').html(info.name);
            $('.jieshao').html(info.detail);
            $('.lianxiren').html('联系人：' + info.contact);
            $('.diqu').html('地区：' + info.area);
            $('.youxiang').html('邮箱：' + info.email);
            $('.dianhua').html('电话：' + info.phone);
            $('.youbian').html('邮编：' + info.zip);
        }

    });

});
