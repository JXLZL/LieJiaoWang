$(function () {
    $('.btn').click(function () {
        var uname = $('.uname').val();
        var pass = $('.password').val();
        $.ajax({
            url:host()+'/login?username='+uname+'&password='+pass,
            type:'post'
        }).done(function (e) {
            Cookies.set('name',e.data.companyName,100);
            Cookies.set('refId',e.data.refId,100);
            Cookies.set('role',e.data.role,100);
            var d = new Date();
            var time = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            Cookies.set('time', time, 100);
            switch (e.data.role) {
                case '管理员':
                    window.location.href = './manage/qiyeguanli.html';
                    break;
                case '买家':
                    $('.login-box').css('display', 'none');
                    $('.login').css('display', 'block');
                    $('.u_name').text('账号：' + e.data.companyName);
                    $('.role').text('身份：' + e.data.role);
                    $('.time').text('最近一次登录时间：' + time);
                    window.location.href='index.html';
                    break;
                case '卖家':
                    $('.u_name').text('账号：' + e.data.companyName);
                    $('.role').text('身份：' + e.data.role);
                    $('.time').text('最近一次登录时间：' + time);
                    window.location.href = './manage/sellerbox1.html';
                    break;
                default :
                    alert("身份出错!");
                    break;
            }
        })
    })
})