/*var cpagenum = Cookies.get('cpage');
 if (cpagenum == undefined) {
 cpagenum = 1
 }
 Cookies.remove('cpage')
 $.ajax({
 url: host() + '/company/pageQuery?pageNo=' + cpagenum + '&pageSize=5',
 }).done(function (e) {
 var clist = e.data.rows;
 var company = '';
 for (var i = 0, len = clist.length; i < len; i++) {
 company += '<li class="clear"><div class="txt"><p>' + clist[i].name + '</p><p>地址：' + clist[i].area + '</p></div></li>'
 }
 $('.q-nav').html(company)
 $('.q-nav li').each(function (d) {
 $(this).click(function () {
 //Cookies.set('company', d + (cpagenum - 1) * 5, 100)
 Cookies.set('companyId', clist[d].id, 100);
 window.location.href = 'qiyejieshao.html';
 })
 })
 $('.page-link').eq(cpagenum - 1).addClass('active')

 $.ajax({
 url: host() + '/company/pageQuery?pageNo=1&pageSize=999',
 }).done(function (d) {
 var p1 = '';
 var pagelist = d.data.rows;
 for (var j = 1, leng = Math.ceil(pagelist.length / 5); j <= leng; j++) {
 p1 += '<li class="page-item"><a class="page-link" num="' + j + '" href="javascript:;">' + j + '</a></li>'
 }
 $('.pagination').html(p1);
 $('.page-link').click(function () {
 var cpage = $(this).attr('num')
 Cookies.set('cpage', cpage, 100)
 window.location.reload();
 })
 })
 });*/

$(function () {
    $.ajax({
        url: host() + '/company/pageQuery?pageNo=1&pageSize=9999',
        success: function (res) {
            if (!res.data.rows) {
                $('.m-l-nav').html('<div class="no_pro">暂未有公司入驻！</div>');
                return false
            }
            //把allList赋值，所有的产品
            allList = res.data.rows;
            //第二部，渲染产品列表，初次渲染
            show(1);

            page();

        }
    });
});

//渲染产品界面函数
function show(e) {
    showList = [];
    //定义长度，换算
    var len = allList.length < e * 5 ? allList.length : e * 5;

    //使用for循环，元素添加到列表里面
    for (var s = (e - 1) * 5; s < len; s++) {
        showList.push(allList[s]);
    }

    //定义一个空的界面，渲染
    var html = '';
    for (var i in showList) {
        html += '<li class="clear"><div class="txt" num="' + showList[i].id + '"><p>' + showList[i].name + '</p><p>地址：' + showList[i].area + '</p></div></li>';
    }
    $('.q-nav').html(html);

    //更新点击事件
    newClick();
}

//更新页码操作
function page() {
    var page = '';
    //页码数量，向上取整，每页是5个产品
    var len = Math.ceil(allList.length / 5);
    //第一步，先把页码渲染出来
    for (var i = 1; i <= len; i++) {
        page += '<li><a class="page-link" num="' + i + '">' + i + '</a></li>'
    }
    $('.pagination').html(page);

    //给每一个页码添加点击事件,num是当前页数
    var num = '';
    $('.page-link').click(function () {
        num = $(this).attr('num');
        show(num);
    })
}

//产品点击事件
function newClick() {
    $('.q-nav li .txt').each(function () {
        $(this).click(function () {
            var info = $(this).attr('num');
            window.location.href = 'qiyejieshao.html?info=' + info;
        });
    });
};



