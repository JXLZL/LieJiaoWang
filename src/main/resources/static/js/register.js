$(function () {

    var str = ['手机号格式错误','√','密码错误','不能为空',''];
    var pattern = /^1[34578]\d{9}$/;
    $('.phone').blur(function () {
        if(!(pattern.test($('.phone').val()))){
            $('.error6').html(str[0]);
        }else{
            $('.error6').html(str[1]);
            $('.error6').css('color',"green")
        }
    })

    $('.password-a').blur(function () {
        if($('.password').val()){
            if($('.password-a').val() == $('.password').val()){
                $('.error4').html(str[1])
                $('.error4').css('color',"green")
            }else{
                $('.error4').html(str[2])
                $('.error4').css('color',"red")
            }
        }
    })

    $('.btn').click(function () {
        if($('.companyName').val()){
            $('.error1').html(str[4])
            if($('.name').val()){
                $('.error2').html(str[4])
                if($('.password').val()){
                    $('.error3').html(str[4])
                    if($('.password-a').val()){
                        if($('.password-a').val() == $('.password').val()){
                            $('.error4').html(str[1]).css('color',"green")
                            if($('.contact').val()){
                                $('.error5').html(str[4])
                                if(!(pattern.test($('.phone').val()))){
                                    $('.error6').html(str[0]);
                                }else{
                                    $('.error6').html(str[1]).css('color',"green")
                                    if($('.email').val()){
                                        $('.error7').html(str[4])
                                        if($('.area').val()){
                                            $('.error8').html(str[4])
                                            if($('.ok').prop('checked')){
                                                //alert($('.name').val());
                                                if($('.role').val()=='买家'){
                                                    $.ajax({
                                                        url:host()+'/buyer/register',
                                                        type:'post',
                                                        dataType:'json',
                                                        contentType:'application/json;charset=utf-8',
                                                        data:JSON.stringify({
                                                            companyName:$('.companyName').val(),
                                                            username:$('.name').val(),
                                                            password:$('.password').val(),
                                                            contact:$('.contact').val(),
                                                            phone:$('.phone').val(),
                                                            email:$('.email').val(),
                                                            id:'',
                                                            area:$('.area').val()
                                                        })
                                                    }).done(function (e) {
                                                        console.log(e.data)
                                                        $.ajax({
                                                            url:host()+'/user/get/'+e.data,
                                                        }).done(function (res) {
                                                            var name = res.data.companyName;
                                                            Cookies.set('name',name,100)
                                                            Cookies.set('refId',res.data.id,100)
                                                            Cookies.set('role','买家')
                                                            var d = new Date();
                                                            var time = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                                                            Cookies.set('time', time, 100);
                                                            window.location.href='index.html'
                                                        })
                                                    })
                                                }else{
                                                    $.ajax({
                                                        url:host()+'/seller/register',
                                                        type:'post',
                                                        dataType:'json',
                                                        contentType:'application/json;charset=utf-8',
                                                        data:JSON.stringify({
                                                            companyName:$('.companyName').val(),
                                                            username:$('.name').val(),
                                                            password:$('.password').val(),
                                                            contact:$('.contact').val(),
                                                            phone:$('.phone').val(),
                                                            email:$('.email').val(),
                                                            id:'',
                                                            area:$('.area').val(),
                                                            audited:false
                                                        })
                                                    }).done(function (e) {
                                                        console.log(e.data)
                                                        $.ajax({
                                                            url:host()+'/user/get/'+e.data,
                                                        }).done(function (p) {
                                                            var name = p.data.companyName;
                                                            Cookies.set('name',name,100)
                                                            Cookies.set('refId',p.data.id,100)
                                                            Cookies.set('role','卖家')
                                                            var d = new Date();
                                                            var time = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                                                            Cookies.set('time', time, 100);
                                                            window.location.href='index.html'
                                                        })
                                                    })
                                                }

                                            }else{
                                                console.log("注册失败")
                                            }
                                        }else{
                                            $('.error8').html(str[3])
                                        }
                                    }else{
                                        $('.error7').html(str[3])
                                    }
                                }
                            }else{
                                $('.error5').html(str[3])
                            }
                        }else{
                            $('.error4').html(str[2]).css('color',"red")
                        }
                    }else{
                        $('.error4').html(str[2])
                    }
                }else{
                    $('.error3').html(str[3])
                }
            }else{
                $('.error2').html(str[3])
            }
        }else{
            $('.error1').html(str[3])
        }

        // if(!a1){
        //     $('.error1').html(str[3])
        // }else{
        //
        // }




        // if(a1&&a2&&a3&&a4&&a5&&a6&&a7&&($('.error4').html()==str[1])&&($('.error6').html())==str[1]){
        //     console.log(1111)
        // }else{
        //     console.log(222222)
        // }



    })



})