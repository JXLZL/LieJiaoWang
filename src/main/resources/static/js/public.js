$(function () {
    //点击返回顶部  回到页面顶部
    $('.fanhui').click(function () {
        $('html,body').animate({
            scrollTop: '0'
        }, 500)
    });
    //根据cookie的字段name 判断是否登录 是则显示退出，否则显示免费注册
    var name = Cookies.get('name');
    if (name) {
        $('.weizhuce').hide();
        $('.yizhuce .h6').html(name);
        $('.yizhuce').show();
    } else {
        $('.yizhuce').hide();
        $('.weizhuce').show();
    }
    //渲染首页的业务分类
    proType();

    //搜索事件
    search();

//    建立搜索类别
    var html = ' <option value="sell">供应产品</option><option value="need">求购产品</option>';
    $('.type').html(html);

    getProList();
    getNewList();

    $('.company').click(function(){
        window.location.href='qiye.html';
    });
    $('.news').click(function(){
        window.location.href='news.html';
    });

    $('.tuichu').click(function () {
        $.ajax({
            url: host() + '/logout',
            type: 'get',
        }).done(function () {
            Cookies.remove('name');
            Cookies.remove('refId');
            Cookies.remove('role');
            window.location.href = '../index.html';
        });
    });
});
function getProList(){

//获取公司的列表 渲染 首页不需要
    $.ajax({
        url: host() + '/company/pageQuery?pageNo=1&pageSize=15',
        success: function (res) {
            var rows = res.data.rows;
            var html = '';
            for (var i in rows) {
                var area = rows[i].area.substring(0,2);
                html += '<li><div calss="left">' + rows[i].name + '</div><span calss="right">' + area + '</span></li>';
            }
            $('.c_nav').html(html);
            //给每一个公司绑定一个点击事件，查看详情
            $('.c_nav li').each(function (e) {
                $(this).click(function () {
                    var id = rows[e].id;
                    //Cookies.set('companyId', id, 100);
                    window.location.href = 'qiyejieshao.html?info=' + id;
                })
            })
        }
    });
}
function getNewList(){

//获取新闻列表
    $.ajax({
        url: host() + '/news/pageQuery?pageNo=1&pageSize=10',
        success: function (res) {
            var rows = res.data.rows;
            var html = '';
            for (var i in rows) {
                var d = new Date(rows[i].createTime);
                var time = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
                html += '<li><div calss="left">' + rows[i].title + '</div><span calss="right right1">' + time + '</span></li>';
            }
            $('.c_nav1').html(html);
            //给每一个新闻添加点击事件，查看详情
            $('.c_nav1 li').each(function (e) {
                $(this).click(function () {
                    var id = rows[e].id;
                    //Cookies.set('newId', id, 100);
                    window.location.href = 'newdetail.html?info=' + id;
                })
            })
        }
    });
}
//网络地址
function host() {
    return '';
    //return 'http://www.liejiaowang.com';
    //return 'http://192.168.1.100:8077';
};

function search() {
    $('.search').click(function () {
        //   点击搜索的时候 如果是求购产品 去求购产品页面，如果是供应产品，去供应产品页面 同时带上搜索的字
        var type = $('.type').val();
        /*var key = $('.key').val();
         Cookies.set('key', key, 100);*/
        var url = '';
        if (type == 'sell') {
            url = 'product.html';
        } else if (type == 'need') {
            url = 'list.html';
        }
        window.location.href = url + '?key=' + $('.key').val();
    });
};
//选取url参数
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
};


function proType() {
    $.ajax({
        url: host() + '/sellType/listAll',
        type: 'GET',
        success: function (res) {
            var typeList = res.data;
            //渲染
            var html = '';
            //添加
            for (var i in typeList) {
                html += '<li class="' + typeList[i].id + '"><i></i>\n' + typeList[i].name + '</li>';
            }
            $('.second-nav').html(html);
            //给产品分类添加点击事件
            $('.second-nav li').each(function () {
                $(this).click(function () {
                    console.log($(this).attr('class'));
                    window.location.href = 'product.html?typeId=' + $(this).attr('class');
                })
            })
        }
    });
}
