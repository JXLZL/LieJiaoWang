$(function () {
    /* var info = Cookies.get('info');
     Cookies.remove('info');*/
    var id = getQueryString("info");
    $.ajax({
        url: host() + '/productBuy/get/' + id,
        success: function (res) {
            var pdetail = res.data;
            var imgurl = host() + pdetail.imageUrl;
            $('.img1').attr('src', imgurl);
            $('.pic img').attr('src', imgurl);
            $('.m-t-left h3').html(pdetail.name);
            $('.price i').html(pdetail.price);
            $('.desc i').html(pdetail.description);
            $('.pack i').html(pdetail.pack);
            $('.xuqiu i').html(pdetail.number);
            $('.phone i').html(pdetail.phone);
            $('.qq i').html(pdetail.qq);
            $('.key i').html(pdetail.key);
            $('.time i').html(pdetail.createTime.substr('0', 10));
            $('.content').html(pdetail.content);
            $('.nums').html(pdetail.visits+'次');
            //获取买家信息
            $.ajax({
                url: host() + '/buyer/get/' + pdetail.buyerId,
                success: function (res) {
                    var comp = res.data;
                    $('.comp').html(comp.companyName);
                    $('.contact b').html(comp.contact);
                    $('.email b').html(comp.email);
                    $('.phone b').html(comp.phone);
                    $('.area b').html(comp.area)
                }
            });

            //浏览次数添加
            $.ajax({
                url: host() + '/productBuy/visit/' + id,
                type: 'PUT',
                success: function (res) {
                    console.log(res);
                }
            })
        }
    });
})