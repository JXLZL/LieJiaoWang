//全局变量 所有的列表
var allList = [];
//var typeList = [];
//定义一个空的列表
var showList = [];
$(function () {
    var key = getQueryString('key');
    if (key) {
        getSearch(key);
        $('.key').val(key);
        return false
    }

    var typeId = getQueryString('typeId');
    if (typeId) {
        //根据类型id获取产品
        var url = host() + '/productSell/pageQuery?pageNo=1&pageSize=9999&typeId=' + typeId;
        $.ajax({
            url: url,
            type: 'GET',
            success: function (res) {
                console.log(res.data);
                if (!res.data.rows) {
                    $('.m-l-nav').html('<div class="no_pro">没有产品!</div>');
                    return false
                }
                allList = res.data.rows;
                show(1);
                //更新页码
                page();

            }
        });
        //赋值选中
        $.ajax({
            url: host() + '/sellType/listAll',
            type: 'GET',
            success: function (res) {
                var typeList = res.data;
                //渲染
                var html = '';
                //添加所有
                html += '<li class="0">' + '所有' + '</li>';
                for (var i in typeList) {
                    if (typeList[i].id == typeId) {
                        html += '<li class="active ' + typeList[i].id + '">' + typeList[i].name + '</li>';
                    } else {
                        html += '<li class="' + typeList[i].id + '">' + typeList[i].name + '</li>';
                    }
                }
                $('.top-nav').html(html);
                //给产品分类添加点击事件
                addClick();
            }
        });
        return false
    }

    //先获取所有的产品
    $.ajax({
        url: host() + '/productSell/pageQuery?pageNo=1&pageSize=9999',
        success: function (res) {
            if (!res.data.rows) {
                $('.m-l-nav').html('<div class="no_pro">没有产品!</div>');
                return false
            }
            //把allList赋值，所有的产品
            allList = res.data.rows;

            //第二部，渲染产品列表，初次渲染
            show(1);

            page();

        }
    });

    //获取产品分类，渲染
    $.ajax({
        url: host() + '/sellType/listAll',
        type: 'GET',
        success: function (res) {
            var typeList = res.data;
            //渲染
            var html = '';
            //添加所有
            html += '<li class="' + 0 + ' active">' + '所有' + '</li>';
            for (var i in typeList) {
                html += '<li class="' + typeList[i].id + '">' + typeList[i].name + '</li>';
            }
            $('.top-nav').html(html);

            //给产品分类添加点击事件
            addClick();
        }
    });

    //搜索，根据名字模糊搜索
    $('.search').click(function () {
        var key = $('.key').val();
        getSearch(key);
    });
});
//渲染产品界面函数
function show(e) {
    showList = [];
    //定义长度，换算
    var len = allList.length < e * 4 ? allList.length : e * 4;

    //使用for循环，元素添加到列表里面
    for (var s = (e - 1) * 4; s < len; s++) {
        showList.push(allList[s]);
    }

    //定义一个空的界面，渲染
    var html = '';
    for (var i in showList) {
        html += '<li class="clear" num="' + i + '">\n' + '<img src="' + host() + showList[i].imageUrl + '">\n' + '<div class="n-txt" num="' +
            showList[i].id + '">\n' + '<p>' + showList[i].name + '</p>\n' + '<p>' + showList[i].description + '</p>\n' + '<p>' + showList[i].key +
            '</p>\n' + '<p class="company"></p>\n' + '<span>\n' + showList[i].createTime.substr('0', 10) + '<b class="' + showList[i].id + ' ' + showList[i].phone + '"></b>\n' +
            '<em class="' + showList[i].id + ' ' + showList[i].qq + '"></em>\n' + '</span>\n' + '<div class="notice p' + showList[i].id + '"></div></div>\n' + '<div class="btn">\n' + '<a class="mianyi">\n' +
            '面议\n' + '</a>\n' + '<a class="xunjia">\n' + ' 询价\n' + '</a>\n' + '</div>\n' + '</li>';
    }
    $('.m-l-nav').html(html);

    //    最后渲染公司名字
    for (var j in showList) {
        if (showList[j].sellerId == 'liejiao') {
            $('.company').html('官方供应');
        } else {
            $.ajax({
                url: host() + '/seller/get/' + showList[j].sellerId,
                success: function (res) {
                    $('.company').html(res.data.companyName);
                }
            })
        }
    }

    //更新点击事件
    newClick();
}

//更新页码操作
function page() {
    var page = '';
    //页码数量，向上取整，每页是4个产品
    var len = Math.ceil(allList.length / 4);
    //第一步，先把页码渲染出来
    for (var i = 1; i <= len; i++) {
        page += '<li><a class="page-link" num="' + i + '">' + i + '</a></li>'
    }
    $('.pagination').html(page);

    //给每一个页码添加点击事件,num是当前页数
    var num = '';
    $('.page-link').click(function () {
        num = $(this).attr('num');
        show(num);
    })
}

//产品点击事件
function newClick() {
    $('.m-l-nav li .n-txt').each(function () {
        $(this).click(function () {
            var info = $(this).attr('num');
            window.location.href = 'product-detail.html?info='+ info;
        });
        $('.notice').css('display', 'none');
    });
    //移入事件 显示电话号码
    $('.m-l-nav li b').each(function () {
        $(this).mouseenter(function () {
            var phone = $(this).attr('class').split(' ')[1];
            var id = $(this).attr('class').split(' ')[0];
            $('.p' + id).css('display', 'block').html(phone);
        });
        $(this).mouseleave(function () {
            $('.notice').css('display', 'none');
        })
    });
    //移入事件 显示QQ
    $('.m-l-nav li em').each(function () {
        $(this).mouseenter(function () {
            var qq = $(this).attr('class').split(' ')[1];
            var id = $(this).attr('class').split(' ')[0];
            $('.p' + id).css('display', 'block').html(qq);
        });
        $(this).mouseleave(function () {
            $('.notice').css('display', 'none');
        })
    })
};

function getSearch(e) {
    var url = host() + '/productSell/search';
    /*if(type == 'sell'){
     url = host()+'/productSell/search';
     }else if(type == 'need'){
     url = host()+'/productBuy/search';
     }*/
    $.ajax({
        url: url,
        type: 'GET',
        data: {
            key: e,
            pageNo: 1,
            pageSize: 99
        },
        success: function (res) {
            console.log(res.data);
            if (!res.data.rows) {
                $('.m-l-nav').html('<div class="no_pro">没有产品!</div>');
                return false
            }

            allList = res.data.rows;
            show(1);

            page();
        }
    })
};

function addClick() {
    $('.top-nav li').each(function () {
        $(this).click(function () {
            $(this).addClass('active').siblings().removeClass('active');
            var typeId = $(this).attr('class').split(' ')[0];
            //如果选的时候，点击所有，url地址更换
            var url = host() + '/productSell/pageQuery?pageNo=1&pageSize=9999&typeId=' + typeId;
            if (typeId == 0) {
                url = host() + '/productSell/pageQuery?pageNo=1&pageSize=9999';
            }
            $.ajax({
                url: url,
                type: 'GET',
                success: function (res) {
                    console.log(res.data);
                    if (!res.data.rows) {
                        $('.m-l-nav').html('<div class="no_pro">没有产品!</div>');
                        return false
                    }
                    allList = res.data.rows;
                    show(1);
                    //更新页码
                    page();

                }
            })
        })
    })
}