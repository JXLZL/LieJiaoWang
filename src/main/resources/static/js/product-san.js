$(function () {

    var pagenum4 = Cookies.get('page4');
    if (pagenum4 == undefined) {
        pagenum4 = 1
    }
    Cookies.remove('page4')

    //获取产品分类
    $.ajax({
        url: host() + '/sellType/listAll',
        type: 'GET',
        success: function (res) {
            var list = res.data;
            var html = '';
            for (var i in list) {
                html += '<li class="' + list[i].id + '">' + list[i].name + '</li>';
            }
            $('.top-nav').html(html);
                $('.top-nav li').each(function () {
                $(this).click(function () {
                    var typeId = $(this).attr('class');
                    $.ajax({
                        //url:host()+'/productSell/pageQuery',
                        url:host()+'/productSell/pageQuery?pageNo=' + pagenum4 + '&pageSize=4&typeId='+typeId,
                        type:'GET',
                        /*data:{
                            pageNo:1,
                            pageSize:4,
                            typeId:typeId
                        },*/
                        success: function(res){
                            console.log(res);
                            var plist = e.data.rows;
                            var html = '';
                            for (var i = 0, len = plist.length; i < len; i++) {
                                var name = '';
                                if (plist[i].sellerId == 'liejiao') {
                                    name='官方供应';
                                } else {
                                    $.ajax({
                                        url: host() + '/seller/get/' + plist[i].sellerId,
                                    }).done(function (d) {
                                        name=d.data.companyName;
                                    })
                                }
                                html += '<li class="clear" num="' + i + '">\n' +
                                    '                        <img src="' + host() + '' + plist[i].imageUrl + '">\n' +
                                    '                        <div class="n-txt">\n' +
                                    '                            <p>' + plist[i].name + '</p>\n' +
                                    '                            <p>' + plist[i].description + '</p>\n' +
                                    '                            <p>' + plist[i].key + '</p>\n' +
                                    '                            <p class="company">'+name+'</p>\n' +
                                    '                            <span>\n' + plist[i].createTime.substr('0', 10) +
                                    '                                <b></b>\n' +
                                    '                                <em></em>\n' +
                                    '                            </span>\n' +
                                    '                        </div>\n' +
                                    '                        <div class="btn">\n' +
                                    '                            <a class="mianyi">\n' +
                                    '                                面议\n' +
                                    '                            </a>\n' +
                                    '                            <a class="xunjia">\n' +
                                    '                                询价\n' +
                                    '                            </a>\n' +
                                    '                        </div>\n' +
                                    '                    </li>'
                            }
                            $('.m-l-nav').html(html);

                        }
                    })
                })
            })
        }
    });

    //渲染
    $.ajax({
        url: host() + '/productSell/pageQuery?pageNo=' + pagenum4 + '&pageSize=4&type=0',
    }).done(function (e) {
        var plist = e.data.rows;
        var html = '';
        for (var i = 0, len = plist.length; i < len; i++) {
            var name = '';
            if (plist[i].sellerId == 'liejiao') {
                name='官方供应';
            } else {
                $.ajax({
                    url: host() + '/seller/get/' + plist[i].sellerId,
                }).done(function (d) {
                    name=d.data.companyName;
                })
            }
            html += '<li class="clear" num="' + i + '">\n' +
                '                        <img src="' + host() + '' + plist[i].imageUrl + '">\n' +
                '                        <div class="n-txt">\n' +
                '                            <p>' + plist[i].name + '</p>\n' +
                '                            <p>' + plist[i].description + '</p>\n' +
                '                            <p>' + plist[i].key + '</p>\n' +
                '                            <p class="company">'+name+'</p>\n' +
                '                            <span>\n' + plist[i].createTime.substr('0', 10) +
                '                                <b></b>\n' +
                '                                <em></em>\n' +
                '                            </span>\n' +
                '                        </div>\n' +
                '                        <div class="btn">\n' +
                '                            <a class="mianyi">\n' +
                '                                面议\n' +
                '                            </a>\n' +
                '                            <a class="xunjia">\n' +
                '                                询价\n' +
                '                            </a>\n' +
                '                        </div>\n' +
                '                    </li>'
        }
        $('.m-l-nav').html(html);

    });

    //渲染底部页码
    $.ajax({
        url: host() + '/productSell/pageQuery?pageNo=1&pageSize=999&type=0'
    }).done(function (d) {
        var p1 = '';
        var pagelist = d.data.rows;
        for (var j = 1, leng = Math.ceil(pagelist.length / 4); j <= leng; j++) {
            p1 += '<li><a class="page-link" num="' + j + '" href="javascript:;">' + j + '</a></li>'
        }
        $('.pagination').html(p1);
        var page4 = '';
        $('.page-link').click(function () {
            page4 = $(this).attr('num')
            Cookies.set('page4', page4, 100);
            window.location.reload();
        })
    });

    $('.m-l-nav li').each(function () {
        $(this).click(function () {
            n = $(this).attr('num')
            var info4 = plist[n].id;
            // console.log(info2)
            Cookies.set('info', info4, 100)
            window.location.href = 'product-detail.html';
        })
    })

});