/*
$(function () {

    var pagenum5 = Cookies.get('page5');
    if(pagenum5==undefined){
        pagenum5=1
    }
    Cookies.remove('page5')

    $.ajax({
        url:host()+'/productBuy/pageQuery?pageNo='+pagenum5+'&pageSize=4',
    }).done(function (e) {
        var plist = e.data.rows;
        // console.log(plist)
        var html = '';
        var gongsi =[];
        for(var i=0,len=plist.length;i<len;i++){
            html += '<li class="clear" num="'+i+'">\n' +
                '                        <img src="'+host()+''+plist[i].imageUrl+'">\n' +
                '                        <div class="n-txt">\n' +
                '                            <p>'+plist[i].name+'</p>\n' +
                '                            <p>'+plist[i].description+'</p>\n' +
                '                            <p>'+plist[i].key+'</p>\n' +
                '                            <p class="company"></p>\n' +
                '                            <span>\n'+plist[i].createTime.substr('0',10) +
                '                                <b></b>\n' +
                '                                <em></em>\n' +
                '                            </span>\n' +
                '                        </div>\n' +
                '                        <div class="btn">\n' +
                '                            <a class="mianyi">\n' +
                '                                面议\n' +
                '                            </a>\n' +
                '                            <a class="xunjia">\n' +
                '                                询价\n' +
                '                            </a>\n' +
                '                        </div>\n' +
                '                    </li>'

            $.ajax({
                url:host()+'/buyer/get/'+plist[i].buyerId,
            }).done(function (d) {
                // console.log(d.data)
                // $('.company').html(d.data.companyName)
                gongsi.push(d.data.companyName)
            })
        }
        console.log(gongsi)

        $('.m-l-nav').html(html)

        $.ajax({
            url:host()+'/productBuy/pageQuery?pageNo=1&pageSize=999',
        }).done(function (d) {
            var p1 = '';
            var pagelist = d.data.rows;
            for(var j=1,leng=Math.ceil(pagelist.length/4);j<=leng;j++){
                p1 +='<li><a class="page-link" num="'+j+'" href="javascript:;">'+j+'</a></li>'
            }
            $('.pagination').html(p1);
            var page5 = '';
            $('.page-link').click(function () {
                page5 = $(this).attr('num')
                Cookies.set('page5',page5,100);
                window.location.reload();
            })
        })

        $('.m-l-nav li').each(function () {
            $(this).click(function () {
                n=$(this).attr('num')
                var info5 = plist[n].id;
                Cookies.set('info',info5,100)
                window.location.href='list-detail.html';
            })
        })
    })

})*/
//全局变量 所有的列表
var allList = [];
//var typeList = [];
//定义一个空的列表
var showList = [];
$(function () {
    document.getElementById('type').value="need";
    var key = getQueryString('key');
    if (key) {
        getSearch(key);
        $('.key').val(key);
        return false
    }

    //先获取所有的产品
    $.ajax({
        url: host() + '/productBuy/pageQuery?pageNo=1&pageSize=9999',
        success: function (res) {
            if (!res.data.rows) {
                $('.m-l-nav').html('<div class="no_pro">没有产品!</div>');
                return false
            }
            //把allList赋值，所有的产品
            allList = res.data.rows;

            //第二部，渲染产品列表，初次渲染
            show(1);

            page();

        }
    });

    //搜索，根据名字模糊搜索
    $('.search').click(function () {
        var key = $('.key').val();
        getSearch(key);
    });
});
//渲染产品界面函数
function show(e) {
    showList = [];
    //定义长度，换算
    var len = allList.length < e * 4 ? allList.length : e * 4;

    //使用for循环，元素添加到列表里面
    for (var s = (e - 1) * 4; s < len; s++) {
        showList.push(allList[s]);
    }

    //定义一个空的界面，渲染
    var html = '';
    for (var i in showList) {
        html += '<li class="clear" num="' + i + '">\n' + '<img src="' + host() + showList[i].imageUrl + '">\n' + '<div class="n-txt" num="' +
            showList[i].id + '">\n' + '<p>' + showList[i].name + '</p>\n' + '<p>' + showList[i].description + '</p>\n' + '<p>' + showList[i].key +
            '</p>\n' + '<p class="company"></p>\n' + '<span>\n' + showList[i].createTime.substr('0', 10) + '<b class="' + showList[i].id + ' ' + showList[i].phone + '"></b>\n' +
            '<em class="' + showList[i].id + ' ' + showList[i].qq + '"></em>\n' + '</span>\n' + '<div class="notice p' + showList[i].id + '"></div></div>\n' + '<div class="btn">\n' + '<a class="mianyi">\n' +
            '面议\n' + '</a>\n' + '<a class="xunjia">\n' + ' 询价\n' + '</a>\n' + '</div>\n' + '</li>';
    }
    $('.m-l-nav').html(html);

    //    最后渲染公司名字
    for (var j in showList) {
        if (showList[j].buyerId == 'liejiao') {
            $('.company').html('官方供应');
        } else {
            $.ajax({
                url: host() + '/buyer/get/' + showList[j].buyerId,
                success: function (res) {
                    $('.company').html(res.data.companyName);
                }
            })
        }
    }

    //更新点击事件
    newClick();
}

//更新页码操作
function page() {
    var page = '';
    //页码数量，向上取整，每页是4个产品
    var len = Math.ceil(allList.length / 4);
    //第一步，先把页码渲染出来
    for (var i = 1; i <= len; i++) {
        page += '<li><a class="page-link" num="' + i + '">' + i + '</a></li>'
    }
    $('.pagination').html(page);

    //给每一个页码添加点击事件,num是当前页数
    var num = '';
    $('.page-link').click(function () {
        num = $(this).attr('num');
        show(num);
    })
}

//产品点击事件
function newClick() {
    $('.m-l-nav li .n-txt').each(function () {
        $(this).click(function () {
            var info = $(this).attr('num');
            window.location.href = 'list-detail.html?info='+info;
        });
        $('.notice').css('display', 'none');
    });
    //移入事件 显示电话号码
    $('.m-l-nav li b').each(function () {
        $(this).mouseenter(function () {
            var phone = $(this).attr('class').split(' ')[1];
            var id = $(this).attr('class').split(' ')[0];
            $('.p' + id).css('display', 'block').html(phone);
        });
        $(this).mouseleave(function () {
            $('.notice').css('display', 'none');
        })
    });
    //移入事件 显示QQ
    $('.m-l-nav li em').each(function () {
        $(this).mouseenter(function () {
            var qq = $(this).attr('class').split(' ')[1];
            var id = $(this).attr('class').split(' ')[0];
            $('.p' + id).css('display', 'block').html(qq);
        });
        $(this).mouseleave(function () {
            $('.notice').css('display', 'none');
        })
    })
};

function getSearch(e) {
    var url = host() + '/productBuy/search';
    /*if(type == 'sell'){
     url = host()+'/productSell/search';
     }else if(type == 'need'){
     url = host()+'/productBuy/search';
     }*/
    $.ajax({
        url: url,
        type: 'GET',
        data: {
            key: e,
            pageNo: 1,
            pageSize: 99
        },
        success: function (res) {
            console.log(res.data);
            if (!res.data.rows) {
                $('.m-l-nav').html('<div class="no_pro">没有产品!</div>');
                return false
            }

            allList = res.data.rows;
            show(1);

            page();
        }
    })
};
