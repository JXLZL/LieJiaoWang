$(function () {
    var id = getQueryString('info');
    var allList = [];
    //获取所有的新闻列表
    $.ajax({
        url: host() + '/news/pageQuery?pageNo=1&pageSize=9999',
        success: function (res) {
            if (!res.data.rows) {
                $('.m-l-nav').html('<div class="no_pro">没有新闻！</div>');
                return false
            }
            //把allList赋值，所有的产品
            allList = res.data.rows;
            console.log(allList);
            $.ajax({
                url: host() + '/news/get/' + id,
                success: function (e) {
                    //增加浏览次数
                    $.ajax({
                        url: host()+'/news/visit/'+ id,
                        type:'PUT',
                        success: function(res){
                        }
                    });
                    //
                    var info = e.data;
                    $('.news-detail h4').html(info.title);
                    var d = new Date(info.createTime);
                    var time = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
                    $('.date').html('发布日期：' + time + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;浏览次数：'+ (info.visits+'次'));
                    $('.news-info').html(info.content);

                    for (var i in allList) {
                        if(i == 0 & allList[i].id == id){
                            i = Number(i);
                            var nextId = allList[i + 1].id;
                            var nextName = allList[i + 1].title;
                            $('.next').html('<i>下一篇：</i>' + nextName);
                            break;
                        }

                        if(allList[i].id == id & i == allList.length - 1){
                            i = Number(i);
                            var prevId = allList[i - 1].id;
                            var prevName = allList[i - 1].title;
                            $('.prev').html('<i>上一篇：</i>' + prevName);
                            break;
                        }

                        if (allList[i].id == id) {
                            i = Number(i);
                            var prevId = allList[i - 1].id;
                            var nextId = allList[i + 1].id;
                            var prevName = allList[i - 1].title;
                            var nextName = allList[i + 1].title;
                            $('.next').html('<i>下一篇：</i>' + nextName);
                            $('.prev').html('<i>上一篇：</i>' + prevName);
                            break;
                        }

                    }

                    $('.next').click(function () {
                        //Cookies.set('newId', nextId, 100);
                        //window.location.reload();
                        window.location.href = 'newdetail.html?info=' + nextId;
                    });

                    $('.prev').click(function () {
                        //Cookies.set('newId', prevId, 100);
                        //window.location.reload();
                        window.location.href = 'newdetail.html?info=' + prevId;
                    });

                }
            });
        }
    });
});