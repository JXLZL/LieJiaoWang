$(function () {

    var pagenum2 = Cookies.get('page2');
    if(pagenum2==undefined){
        pagenum2=1
    }
    Cookies.remove('page2')

    $.ajax({
        url:host()+'/productSell/pageQuery?pageNo='+pagenum2+'&pageSize=4&type=2',
    }).done(function (e) {
        var plist = e.data.rows;
        var html = '';
        for(var i=0,len=plist.length;i<len;i++){
            html += '<li class="clear" num="'+i+'">\n' +
                '                        <img src="'+host()+''+plist[i].imageUrl+'">\n' +
                '                        <div class="n-txt">\n' +
                '                            <p>'+plist[i].name+'</p>\n' +
                '                            <p>'+plist[i].description+'</p>\n' +
                '                            <p>'+plist[i].key+'</p>\n' +
                '                            <p class="company"></p>\n' +
                '                            <span>\n'+plist[i].createTime.substr('0',10) +
                '                                <b></b>\n' +
                '                                <em></em>\n' +
                '                            </span>\n' +
                '                        </div>\n' +
                '                        <div class="btn">\n' +
                '                            <a class="mianyi">\n' +
                '                                面议\n' +
                '                            </a>\n' +
                '                            <a class="xunjia">\n' +
                '                                询价\n' +
                '                            </a>\n' +
                '                        </div>\n' +
                '                    </li>'

            $.ajax({
                url:host()+'/seller/get/'+plist[i].sellerId,
            }).done(function (d) {
                $('.company').html(d.data.companyName)
            })
        }
        $('.m-l-nav').html(html)

        $.ajax({
            url:host()+'/productSell/pageQuery?pageNo=1&pageSize=999&type=2',
        }).done(function (d) {
            var p1 = '';
            var pagelist = d.data.rows;
            for(var j=1,leng=Math.ceil(pagelist.length/4);j<=leng;j++){
                p1 +='<li><a class="page-link" num="'+j+'" href="javascript:;">'+j+'</a></li>'
            }
            $('.pagination').html(p1);
            var page2 = '';
            $('.page-link').click(function () {
                page2 = $(this).attr('num')
                Cookies.set('page2',page2,100);
                window.location.reload();
            })
        })

        $('.m-l-nav li').each(function () {
            $(this).click(function () {
                n=$(this).attr('num')
                var info2 = plist[n].id;
                // console.log(info2)
                Cookies.set('info',info2,100)
                window.location.href='product-detail.html';
            })
        })
    })

})