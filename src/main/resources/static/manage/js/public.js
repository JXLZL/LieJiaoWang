$(function () {
    //动态创建一个修改密码
    var html ='<div class="new_psd">新密码：<input type="text"/></div> <div class="confirm_psd">确认密码：<input type="text"/></div> <input class="sub" type="submit" value="提交"/>';
    var div = document.createElement('div');
    var divattr = document.createAttribute("class");
    divattr.value = "psd";
    div.setAttributeNode(divattr);
    document.getElementsByTagName("body").item(0).appendChild(div);
    $('.psd').html(html).css('display','none');

    //图片
    UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
    UE.Editor.prototype.getActionUrl = function (action) {
        if (action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage') {
            return host() + "/image/ueditor"; //在这里返回我们实际的上传图片地址
        } else {
            return this._bkGetActionUrl.call(this, action);
        }
    };

    var bstop = 1;
    if (bstop == 2) {
        $('.d-txt').show();
        $('.d-box').find('tbody').hide()
    } else {
        $('.d-txt').hide();
        $('.d-box').find('tbody').show()
    }

    var check = $('.d-box thead .select input').prop('checked');
    $('.d-box thead .select input').click(function () {
        console.log(check)
        if (check == true) {
            console.log('取消全选')
            $('.d-box thead .select input').prop('checked', false);
            check = $('.d-box thead .select input').prop('checked');
            $('.d-box tbody .select input').each(function () {
                $(this).prop('checked', false)
            })
        } else {
            console.log('全选')
            $('.d-box thead .select input').prop('checked', true);
            check = $('.d-box thead .select input').prop('checked');
            $('.d-box tbody .select input').each(function () {
                $(this).prop('checked', true)
            })
        }
    });

    $('.d-box tbody .select input').each(function () {
        $(this).click(function () {
            console.log($(this).prop('checked'))
            if ($(this).prop('checked') == false) {
                $('.d-box thead .select input').prop('checked', false);
                check = $('.d-box thead .select input').prop('checked');
                $(this).prop('checked', false)
            } else {
                $(this).prop('checked', true)
            }
        })
    });

    $('.header h1').click(function () {
        window.location.href = '../index.html';
    });
    $('.tuichu').click(function () {
        $.ajax({
            url: host() + '/logout',
            type: 'get',
        }).done(function () {
            Cookies.remove('name');
            Cookies.remove('refId');
            Cookies.remove('role');
            window.location.href = '../index.html';
        });
    });
    // $('.del a').click(function () {
    //     var move = '您确认要删除吗？';
    //     if(confirm(move) == true){
    //         $(this).parents('tr').remove();
    //     }else{
    //         return false;
    //     }
    // })

    // $('.m-btn a:eq(1)').click(function () {
    //     console.log(22222222)
    //     var move = '您确认要删除吗？';
    //     if(confirm(move) == true) {
    //         $('tbody .select input:checked').each(function () {
    //             n = $(this).parent('th').parent('tr').attr('num');
    //             console.log(n)
    //             $('.d-box tbody').find('tr:eq(' + n + ')').remove();
    //         })
    //         window.location.reload();
    //     }else{
    //         return false;
    //     }
    // })
    $('.chongzhi').click(function(){
        $('.psd').css('display','block');
    });

    $('.sub').click(function(){
        var id= Cookies.get('userId');
        var psd1 = $('.new_psd input').val();
        var psd2 = $('.confirm_psd input').val();
        if(psd1 != psd2){
            alert("密码不一致");
            return false;
        }
        $.ajax({
            url: host() + '/user/resetPassword',
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                id:id,
                password:psd1,
                username:Cookies.get('name')
            }),
            success: function(res){
                if(res.errors){
                    alert(res.errors[0].message);
                    return false
                }
                alert('修改密码成功!');
                $('.psd').css('display','none');
            }
        })
    })
});

function host() {
    return '';
    //return 'http://www.liejiaowang.com';
    //return 'http://192.168.1.100:8077';
}

//选取url参数
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
};