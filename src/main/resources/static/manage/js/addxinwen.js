$(function () {
    var editId = Cookies.get('edit2');
    var editor = UE.getEditor('editor');

    /*UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
    UE.Editor.prototype.getActionUrl = function (action) {
        if (action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage') {
            return host() + "/image/upload?module=ueditor"; //在这里返回我们实际的上传图片地址
        } else {
            return this._bkGetActionUrl.call(this, action);
        }
    }*/
    if(editId==undefined){
        $('.quxiao').click(function () {
            window.location.href='xinwenguanli.html'
        })

        //头像预览
        $("#headPic").click(function () {
            $("#image").click(); //隐藏了input:file样式后，点击头像就可以本地上传
            $("#image").on("change",function(){
                var objUrl = getObjectURL(this.files[0]) ; //获取图片的路径，该路径不是图片在本地的路径
                if (objUrl) {
                    $("#headPic").attr("src", objUrl) ; //将图片路径存入src中，显示出图片
                }
            });
        });
        $("#image").on("change",function(){
            var objUrl = getObjectURL(this.files[0]) ; //获取图片的路径，该路径不是图片在本地的路径
            if (objUrl) {
                $("#headPic").attr("src", objUrl) ; //将图片路径存入src中，显示出图片
            }
        });

        //图片上传
        $('#submit_btn').click(function () {
            var formData = new FormData();

            var image = $('input[name=image]')[0].files[0];
            formData.append('image', image);
            formData.append('module', 'news');

            // console.log("这是获取的图片：" + image);
            $.ajax({
                url: host()+'/image/upload',
                type: "POST",
                contentType: false,
                processData: false,
                async: false,
                dataType: "JSON",
                data: formData,
                success: function (result,error,s) {
                    if(s){
                        if(result.data=={}){

                        }else{
                        }
                    }else{
                        alert('上传失败');
                    }
                }
            }).done(function (d) {
                $('.imageUrl').val(d.data.url)
            })
        });

        //建立一個可存取到該file的url
        function getObjectURL(file) {
            var url = null ;
            if (window.createObjectURL!=undefined) { // basic
                url = window.createObjectURL(file) ;
            } else if (window.URL!=undefined) { // mozilla(firefox)
                url = window.URL.createObjectURL(file) ;
            } else if (window.webkitURL!=undefined) { // webkit or chrome
                url = window.webkitURL.createObjectURL(file) ;
            }
            return url ;
        }

        $('.baocun').click(function () {
            var description = editor.getContent();
            $.ajax({
                url:host()+'/news/add',
                type:'post',
                dataType:'json',
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify({
                    title:$('.title').val(),
                    author:$('.author').val(),
                    intro:$('.intro').val(),
                    source:$('.source').val(),
                    content:description,
                    id:'',
                    // readCount:'',
                    // comment:'',
                    imageUrl:$('.imageUrl').val()
                })
            }).done(function (res) {
                $('.xinjia').animate({
                    opacity:'0.8'
                }).animate({
                    opacity:'0'
                },2000)
                if(!res.errors){
                    window.location.href='xinwenguanli.html';
                }
            })
        })

    }else{
        Cookies.remove('edit2');
        $('#submit_btn').hide();
        $('.list').eq(1).hide();
        $.ajax({
            url:host()+'/news/get/'+editId,
        }).done(function (e) {
            var info = e.data;
            $('.title').val(info.title);
            $('.author').val(info.author);
            $('.intro').val(info.intro);
            $('.source').val(info.source);
            //$('.content').val(info.content);
            $('.imageUrl').val(info.imageUrl);
            editor.ready(function () {
                editor.setContent(info.content);
            });
            $('.quxiao').click(function () {
                window.location.href='xinwenguanli.html'
            })
            $('.baocun').click(function () {
                var description = editor.getContent();

                $.ajax({
                    url:host()+'/news/update',
                    type:'put',
                    dataType:'json',
                    contentType:'application/json;charset=utf-8',
                    data:JSON.stringify({
                        title:$('.title').val(),
                        author:$('.author').val(),
                        intro:$('.intro').val(),
                        source:$('.source').val(),
                        // readCount:'',
                        content:description,
                        id:editId,
                        // comment:'',
                        imageUrl:$('.imageUrl').val()
                    })
                }).done(function (res) {
                    $('.xiugai').animate({
                        opacity:'0.8'
                    }).animate({
                        opacity:'0'
                    },2000);
                    if(!res.errors){
                        window.location.href='xinwenguanli.html';
                    }
                })
            })
        })
    }


})