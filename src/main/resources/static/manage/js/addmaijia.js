$(function () {
    var editId = Cookies.get('edit3');
    if (editId == undefined) {
        // $('.quxiao').click(function () {
        //     window.location.href='maijiaguanli.html'
        // })
        // $('.baocun').click(function () {
        //     $.ajax({
        //         url:host()+'/seller/add',
        //         type:'post',
        //         dataType:'json',
        //         contentType:'application/json;charset=utf-8',
        //         data:JSON.stringify({
        //             username:'',
        //             companyName:$('.companyName').val(),
        //             phone:$('.phone').val(),
        //             password:'',
        //             email:$('.email').val(),
        //             area:$('.area').val(),
        //             contact:$('.contact').val(),
        //             id:''
        //         })
        //     }).done(function () {
        //         $('.xinjia').animate({
        //             opacity:'0.8'
        //         }).animate({
        //             opacity:'0'
        //         },2000)
        //     })
        // })

    } else {

        $.ajax({
            url: host() + '/seller/get/' + editId
        }).done(function (e) {
            var info = e.data;
            $('.companyName').val(info.companyName);
            $('.phone').val(info.phone);
            $('.email').val(info.email);
            $('.area').val(info.area);
            $('.contact').val(info.contact);
            document.getElementById('state').value = info.audited;
            if (info.audited) {
                document.getElementById('state').disabled = 'disabled'
            }
        })
    }
    $('.quxiao').click(function () {
        window.location.href = 'maijiaguanli.html'
    });
    $('.baocun').click(function () {
        $.ajax({
            url: host() + '/seller/update',
            type: 'put',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                username: '',
                companyName: $('.companyName').val(),
                phone: $('.phone').val(),
                password: '',
                email: $('.email').val(),
                area: $('.area').val(),
                contact: $('.contact').val(),
                id: editId
            })
        }).done(function (res) {
            Cookies.remove('edit3');
            $('.xiugai').animate({
                opacity: '0.8'
            }).animate({
                opacity: '0'
            }, 2000);
            //审核
            if (document.getElementById('state').disabled == 'disabled') {
                window.location.href = 'maijiaguanli.html';
            } else {
                $.ajax({
                    url: host() + '/seller/audit/' + editId + '/' + document.getElementById('state').value,
                    type: 'put',
                    dataType: 'json',
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify({})
                }).done(function (res) {
                    console.log(res)
                    window.location.href = 'maijiaguanli.html';
                    return true
                });
                window.location.href = 'maijiaguanli.html';
            }
        })

    })
})