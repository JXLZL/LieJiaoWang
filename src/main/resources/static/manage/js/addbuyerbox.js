$(function () {
    //var editId = Cookies.get('edit11');
    var refId = Cookies.get('refId');
    var role = Cookies.get('role');
    var id = '';
    var password = '';
    var lastLoginIp = '';
    var lastLoginTime = '';
    var username = '';
    var contact = '';
    if (role == '买家') {
        $.ajax({
            url: host() + '/buyer/get/' + refId,
        }).done(function (e) {
            console.log(e.data);
            var info = e.data;
            id = info.id;
            password = info.password;
            lastLoginIp = info.lastLoginIp;
            lastLoginTime = info.lastLoginTime;
            username = info.username;
            contact = info.contact;
            $('.companyName').val(info.companyName);
            $('.phone').val(info.phone);
            $('.email').val(info.email);
        })
    }
    $('.baocun').click(function () {
        $.ajax({
            url: host() + '/buyer/update',
            type: 'put',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                username: username,
                companyName: $('.companyName').val(),
                refId: refId,
                phone: $('.phone').val(),
                password: password,
                email: $('.email').val(),
                role: '买家',
                lastLoginIp: lastLoginIp,
                lastLoginTime: lastLoginTime,
                id: id,
                contact: contact
            })
        }).done(function (res) {
            if(res.errors){
                alert(res.errors[0].message)
                return false
            }
            $('.xiugai').animate({
                opacity: '0.8'
            }).animate({
                opacity: '0'
            }, 2000)
            window.location.reload();
        })
    })
})