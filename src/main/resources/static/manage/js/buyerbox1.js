
var allList = [];
var typeList = [];
var numList = [];
var state = 0;
var p = 1;
var s = 1;
var refId = Cookies.get('refId');
//状态码  1是全选状态，0是取消状态
$(function () {
    //第一步先获取分类，再获取所有产品，同时渲染页码，添加点击事件，渲染产品，添加点击事件。删除事件

    //获取产品分类，渲染
   /* $.ajax({
        url: host() + '/sellType/listAll',
        type: 'GET',
        success: function (res) {
            typeList = res.data;
            //渲染
            var html = '';
            //添加所有
            //html += '<li class="active ' + 0 + '">' + '所有' + '</li>';
            html += '<a class="0 active">所有</a>';
            for (var i in typeList) {
                //html += '<li class="' + typeList[i].id + '">' + typeList[i].name + '</li>';
                html += '<a class="' + typeList[i].id + '">' + typeList[i].name + '</a>';
            }
            $('.choose').html(html);*/

            //获取所有的产品
            $.ajax({
                url: host() + '/productBuy/pageQuery?pageNo=1&pageSize=9999&buyerId=' + refId,
                success: function (res) {
                    if (!res.data.rows) {
                        $('.t_content').html('<div class="no_pro">没有产品!</div>');
                        return false
                    }
                    //把allList赋值，所有的产品
                    allList = res.data.rows;

                    //第二部，渲染产品列表，初次渲染
                    show(1);

                    page();

                }
            });

        //}
    //});

//

});
//渲染产品界面函数
function show(e) {
    showList = [];
    //定义长度，换算
    var len = allList.length < e * 5 ? allList.length : e * 5;

    //使用for循环，元素添加到列表里面
    for (var s = (e - 1) * 5; s < len; s++) {
       /* allList[s].type = '未分类';
        for (var j in typeList) {
            typeList[j].id == allList[s].typeId ? allList[s].type = typeList[j].name : '';
        }*/
        showList.push(allList[s]);
    }

    //定义一个空的界面，渲染
    var html = '';
    for (var i in showList) {

        var reg = /\[.*\]/g;
        var str = showList[i].description;
        if (str != null) {
            str = str.replace(reg, "");
        }

        html += '<tr num="' + showList[i].id + '">\n' +
            '<th class="select" scope="row">\n' +
            '<input type="checkbox">\n' +
            '</th>\n' +
            '<td>' + showList[i].name + '</td>\n' +
            '<td>' + showList[i].price + '</td>\n' +
            '<td>' + showList[i].phone + '</td>\n' +
            '<td>' + showList[i].qq + '</td>\n' +
            '<td>' + showList[i].key + '</td>\n' +
            '<td><img id="imgLi" src="' + host() + showList[i].imageUrl + '" alt=""/></td>\n' +
            //'<td>' + showList[i].type + '</td>\n' +
            '<td class="edit">\n' +
            '<a></a>\n' +
            '</td>\n' +
            '<td class="del">\n' +
            '<a></a>\n' +
            '</td>\n' +
            '</tr>';
    }
    $('.t_content').html(html);

    /*//更新点击事件
     newClick();*/

    //添加单个删除 点击事件
    oneDelete();

    //多个点击删除
    allDelete();

    //编辑事件
    redact();
}

//更新页码操作
function page() {
    var page = '';
    //页码数量，向上取整，每页是4个产品
    var len = Math.ceil(allList.length / 5);
    //第一步，先把页码渲染出来
    for (var i = 1; i <= len; i++) {
        page += '<li><a class="page-link" num="' + i + '">' + i + '</a></li>'
    }
    $('.pagination').html(page);

    //给每一个页码添加点击事件,num是当前页数
    var num = '';
    $('.page-link').click(function () {
        num = $(this).attr('num');
        show(num);
    })
}


//单个点击事件
function oneDelete() {
    $('.d-box .del').each(function () {
        $(this).click(function () {
            var id = $(this).parents('tr').attr('num');
            var move = '您确认要删除吗？';
            if (confirm(move) == true) {
                $.ajax({
                    url: host() + '/productBuy/delete/' + id,
                    type: 'delete',
                    success: function (res) {
                        if (res.errors) {
                            return false
                        }
                        window.location.reload()
                    }
                })
            } else {
                return false;
            }
        })
    })
}
//多个点击事件
function allDelete() {
    //先绑定事件
    $('.d-box tbody .select input').each(function () {
        $(this).click(function () {
            var id = $(this).parents('tr').attr('num');
            console.log($(this).prop('checked'));
            if ($(this).prop('checked') == false) {
                $('.d-box thead .select input').prop('checked', false);
                check = $('.d-box thead .select input').prop('checked');
                $(this).prop('checked', false);
                var index = numList.indexOf(id);
                if (index > -1) {
                    numList.splice(index, 1);
                }
            } else {
                $(this).prop('checked', true);
                numList.push(id);
            }
        })
    });
//    在绑定点击删除事件
    $('.deletes').click(function () {
        if (!s) {
            return false
        }
        s--;
        var move = '您确认要删除0吗？';
        if (confirm(move) == true) {
            for (var i in numList) {
                $.ajax({
                    url: host() + '/productBuy/delete/' + numList[i],
                    type: "delete",
                    success: function (res) {
                        if (res.errors) {
                            return false
                        }
                    }
                })
            }
            window.location.reload();
        } else {
            return false;
        }
    });

    $('.selectAll').click(function () {
        if (!p) {
            return false
        }
        p--;
        //如果是0个  则是取消状态 是3 2 1 0 个 则是选中状态
        if (numList.length == 0) {
            state = 0;
        } else {
            state = 1;
        }
        if (state) {
            numList = [];
            state = 0;
        } else {
            numList = [];
            for (var i in showList) {
                numList.push(showList[i].id);
            }
            state = 1;
        }
    })
}

//编辑事件
function redact() {
    $('.d-box .edit').each(function () {
        $(this).click(function () {
            var id = $(this).parents('tr').attr('num');
            //var edit = allList[n3].id;
            Cookies.set('edit10', id, 100);
            window.location.href = 'addbuyerbox1.html';
        })
    })
}


