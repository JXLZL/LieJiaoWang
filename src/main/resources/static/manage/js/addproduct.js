$(function () {
    $('.quxiao').click(function () {
        window.location.href = 'sellerbox1.html'
    })
    var editor = UE.getEditor('editor');

    /*UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
    UE.Editor.prototype.getActionUrl = function (action) {
        if (action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage') {
            return host() + "/image/upload?module=ueditor"; //在这里返回我们实际的上传图片地址
        } else {
            return this._bkGetActionUrl.call(this, action);
        }
    }*/
    //头像预览
    $("#headPic").click(function () {
        $("#image").click(); //隐藏了input:file样式后，点击头像就可以本地上传
        $("#image").on("change", function () {
            var objUrl = getObjectURL(this.files[0]); //获取图片的路径，该路径不是图片在本地的路径
            if (objUrl) {
                $("#headPic").attr("src", objUrl); //将图片路径存入src中，显示出图片
            }
        });
    });
    $("#image").on("change", function () {
        var objUrl = getObjectURL(this.files[0]); //获取图片的路径，该路径不是图片在本地的路径
        if (objUrl) {
            $("#headPic").attr("src", objUrl); //将图片路径存入src中，显示出图片
        }
    });

    //图片上传
    $('#submit_btn').click(function () {
        var formData = new FormData();

        var image = $('input[name=image]')[0].files[0];
        // var module = $('#module').val();
        formData.append('image', image);
        formData.append('module', 'product');

        // console.log("这是获取的图片：" + image);
        $.ajax({
            url: host() + '/image/upload',
            type: "POST",
            contentType: false,
            processData: false,
            async: false,
            dataType: "JSON",
            data: formData,
            success: function (result, error, s) {
                // var imgurl = result.data.url;
                if (s) {
                    if (result.data == {}) {

                    } else {
                    }
                } else {
                    alert('上传失败');
                }
            }
        }).done(function (d) {
            $('.imageUrl').val(d.data.url)
        })
    });

    //建立一個可存取到該file的url
    function getObjectURL(file) {
        var url = null;
        if (window.createObjectURL != undefined) { // basic
            url = window.createObjectURL(file);
        } else if (window.URL != undefined) { // mozilla(firefox)
            url = window.URL.createObjectURL(file);
        } else if (window.webkitURL != undefined) { // webkit or chrome
            url = window.webkitURL.createObjectURL(file);
        }
        return url;
    }

    //获取份分类
    //获取产品分类
    $.ajax({
        url: host() + '/sellType/listAll',
        type: 'GET'
    }).done(function (res) {
        console.log(res.data);
        var list = res.data;
        var html = '';
        for (var i in list) {
            html += '<option value="' + list[i].id + '">' + list[i].name + '</option>';
        }
        $('.type').html(html);
    });

    // var url = '';
    var refId = Cookies.get('refId');
    $('.baocun').click(function () {
        var type = $('.type').val();
        var description = editor.getContent();
        $.ajax({
            url: host() + '/productSell/add',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                name: $('.name').val(),
                imageUrl: $('.imageUrl').val(),
                sellerId: refId,
                price: $('.price').val(),
                phone: $('.phone').val(),
                qq: $('.qq').val(),
                key: $('.key').val(),
                description: description,
                typeId: type,
                id: ''
            })
        }).done(function (res) {
            if(res.errors){
                alert(res.errors[0].message);
                return false
            }
            $('.xinjia').animate({
                opacity: '0.8'
            }).animate({
                opacity: '0'
            }, 2000);
            window.location.href='sellerbox1.html';
        })

    })


});