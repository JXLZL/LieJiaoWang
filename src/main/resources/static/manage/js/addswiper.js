$(function () {
    $('.quxiao').click(function () {
        Cookies.remove('swiper');
        window.location.href = 'swiper.html'
    });
    var id = Cookies.get('swiper');
    if (id) {
        $.ajax({
            url: host() + '/banner/get/' + id,
            type: 'GET',
            success: function (res) {
                var data = res.data;
                $('.name').val(data.name);
                $('.imageUrl').val(data.imageUrl);
                $('.url').val(data.linkUrl);
                $('.index').val(data.sortIndex);
                $('.imgbox img').attr('src', 'http://192.168.1.101:8077' + data.imageUrl);
            }
        })
    } else {
        $('.sortIndex').css('display', 'none');
    }

    $("#image").on("change", function () {
        var objUrl = getObjectURL(this.files[0]); //获取图片的路径，该路径不是图片在本地的路径
        if (objUrl) {
            $("#headPic").attr("src", objUrl); //将图片路径存入src中，显示出图片
        }
    });

    //图片上传
    $('#submit_btn').click(function () {
        var formData = new FormData();

        var image = $('input[name=image]')[0].files[0];
        formData.append('image', image);
        formData.append('module', 'product');

        // console.log("这是获取的图片：" + image);
        $.ajax({
            url: host() + '/image/upload',
            type: "POST",
            contentType: false,
            processData: false,
            async: false,
            dataType: "JSON",
            data: formData,
            success: function (result, error, s) {
                // var imgurl = result.data.url;
                if (s) {
                    if (result.data == {}) {

                    } else {
                    }
                } else {
                    alert('上传失败');
                }
            }
        }).done(function (d) {
            $('.imageUrl').val(d.data.url)
        })
    });

    //建立一個可存取到該file的url
    function getObjectURL(file) {
        var url = null;
        if (window.createObjectURL != undefined) { // basic
            url = window.createObjectURL(file);
        } else if (window.URL != undefined) { // mozilla(firefox)
            url = window.URL.createObjectURL(file);
        } else if (window.webkitURL != undefined) { // webkit or chrome
            url = window.webkitURL.createObjectURL(file);
        }
        return url;
    }

    if (id) {
        $('.baocun').click(function () {
            /*var formData = new FormData();
             formData.append('id', id);
             formData.append('imageUrl', $('.imageUrl').val());
             formData.append('name', $('.name').val());
             formData.append('linkUrl', $('.url').val());
             formData.append('sortIndex', $('.index').val());
             console.log(id, $('.imageUrl').val(), $('.name').val(), $('.url').val(), $('.index').val());*/
            /* $.ajax({
             url: host() + '/banner/update',
             type: 'PUT',
             contentType: false,
             data: formData,
             dataType: "JSON",
             processData: false,
             async: false,
             success: function (res) {
             console.log(res);
             if (!res.error) {
             $('.xinjia').animate({
             opacity: '0.8'
             }).animate({
             opacity: '0'
             }, 2000);
             window.location.href = 'swiper.html';
             }
             }
             });*/
            $.ajax({
                url: host() + '/banner/update?id='+id+'&imageUrl='+$('.imageUrl').val()+'&name='+$('.name').val()+'&linkUrl='+$('.url').val()+'&sortIndex='+ $('.index').val(),
                type: 'PUT',
                success: function (res) {
                    console.log(res);
                    if (!res.error) {
                        $('.xinjia').animate({
                            opacity: '0.8'
                        }).animate({
                            opacity: '0'
                        }, 2000);
                        Cookies.remove('swiper');
                        window.location.href = 'swiper.html';
                    }
                }
            })
        })
    } else {
        $('.baocun').click(function () {
            var img = $('.imageUrl').val();
            var name = $('.name').val();
            var url = $('.url').val();
            if(!img|!name|!url){
                alert("名字");
                return false;
            }
            var formData = new FormData();
            formData.append('imageUrl', img);
            formData.append('name', name);
            formData.append('linkUrl', url);
            $.ajax({
                url: host() + '/banner/add',
                type: 'POST',
                contentType: false,
                data: formData,
                dataType: "JSON",
                processData: false,
                async: false,
                success: function (res) {
                    if (!res.error) {
                        $('.xinjia').animate({
                            opacity: '0.8'
                        }).animate({
                            opacity: '0'
                        }, 2000);
                        window.location.href = 'swiper.html';
                    }
                }
            })
        })
    }
});