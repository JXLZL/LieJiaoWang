$(function () {
    $('.quxiao').click(function () {
        Cookies.remove('edit10');
        window.location.href = 'productf.html';
    });
    var id = Cookies.get('edit10');
    if (id) {
        $.ajax({
            url: host() + '/sellType/get/' + id
        }).done(function (res) {
            console.log(res.data);
            $('.name').val(res.data.name);
            $('.index').val(res.data.sortIndex);
            $('.baocun').click(function () {
                $.ajax({
                    url: host() + '/sellType/update?id=' + id + '&name=' + $('.name').val() + '&sortIndex=' + $('.index').val(),
                    type: 'put',
                }).done(function (res) {
                    console.log(res);
                    if (res.success) {
                        $('.xinjia').animate({
                            opacity: '0.8'
                        }).animate({
                            opacity: '0'
                        }, 2000);
                        Cookies.remove('edit10');
                        window.location.href = 'productf.html'
                    } else {
                        if (res.errors[0].code) {
                            alert(res.errors[0].message);
                            return false
                        }
                    }
                })
            })
        });
    } else {
        $('.add').css('display', 'none');
        $('.baocun').click(function () {
            $.ajax({
                url: host() + '/sellType/add?name=' + $('.name').val(),
                type: 'post',
            }).done(function () {
                $('.xinjia').animate({
                    opacity: '0.8'
                }).animate({
                    opacity: '0'
                }, 2000);
                window.location.href = 'productf.html'
            })
        })
    }
});