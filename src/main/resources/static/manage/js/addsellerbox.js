$(function () {
    //var editId = Cookies.get('edit11');
    var refId = Cookies.get('refId');
    var role = Cookies.get('role');
    /*refId = 'f8545d409c3e434fad2b78668b05067a';
     role = '买家';*/
    var id = '';
    var password = '';
    var lastLoginIp = '';
    var lastLoginTime = '';
    var username = '';
    var contact = '';
    if (role == '卖家') {
        $.ajax({
            url: host() + '/seller/get/' + refId,
        }).done(function (e) {
            console.log(e.data);
            var info = e.data;
            id = info.id;
            password = info.password;
            lastLoginIp = info.lastLoginIp;
            lastLoginTime = info.lastLoginTime;
            username = info.username;
            contact = info.contact;
            $('.companyName').val(info.companyName);
            $('.phone').val(info.phone);
            $('.email').val(info.email);
        })
    }
    $('.baocun').click(function () {
        $.ajax({
            url: host() + '/seller/update',
            type: 'put',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                username: username,
                companyName: $('.companyName').val(),
                refId: refId,
                phone: $('.phone').val(),
                password: password,
                email: $('.email').val(),
                role: role,
                lastLoginIp: lastLoginIp,
                lastLoginTime: lastLoginTime,
                id: id,
                contact:contact
            })
        }).done(function () {
            $('.xiugai').animate({
                opacity: '0.8'
            }).animate({
                opacity: '0'
            }, 2000)
        })
    })

})