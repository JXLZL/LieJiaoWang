$(function () {
    var editId = Cookies.get('edit10');
    var refId = Cookies.get('refId');

    var editor = UE.getEditor('editor');

    /*UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
    UE.Editor.prototype.getActionUrl = function (action) {
        if (action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage') {
            return host() + "/image/upload?module=ueditor"; //在这里返回我们实际的上传图片地址
        } else {
            return this._bkGetActionUrl.call(this, action);
        }
    }*/
    $('.quxiao').click(function () {
        window.location.href='buyerbox1.html'
    });

    //头像预览
    $("#headPic").click(function () {
        $("#image").click(); //隐藏了input:file样式后，点击头像就可以本地上传
        $("#image").on("change",function(){
            var objUrl = getObjectURL(this.files[0]) ; //获取图片的路径，该路径不是图片在本地的路径
            if (objUrl) {
                $("#headPic").attr("src", objUrl) ; //将图片路径存入src中，显示出图片
            }
        });
    });
    $("#image").on("change",function(){
        var objUrl = getObjectURL(this.files[0]) ; //获取图片的路径，该路径不是图片在本地的路径
        if (objUrl) {
            $("#headPic").attr("src", objUrl) ; //将图片路径存入src中，显示出图片
        }
    });

    //图片上传
    $('#submit_btn').click(function () {
        var formData = new FormData();

        var image = $('input[name=image]')[0].files[0];
        formData.append('image', image);
        formData.append('module', 'product buy');

        // console.log("这是获取的图片：" + image);
        $.ajax({
            url: host()+'/image/upload',
            type: "POST",
            contentType: false,
            processData: false,
            async: false,
            dataType: "JSON",
            data: formData,
            success: function (result,error,s) {
                if(s){
                    if(result.data=={}){

                    }else{
                    }
                }else{
                    alert('上传失败');
                }
            }
        }).done(function (d) {
            $('.imageUrl').val(d.data.url)
        })
    });

    //建立一個可存取到該file的url
    function getObjectURL(file) {
        var url = null ;
        if (window.createObjectURL!=undefined) { // basic
            url = window.createObjectURL(file) ;
        } else if (window.URL!=undefined) { // mozilla(firefox)
            url = window.URL.createObjectURL(file) ;
        } else if (window.webkitURL!=undefined) { // webkit or chrome
            url = window.webkitURL.createObjectURL(file) ;
        }
        return url ;
    }


    if(editId==undefined){
        $('.baocun').click(function () {
            $.ajax({
                url:host()+'/productBuy/add',
                type:'post',
                dataType:'json',
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify({
                    name:$('.name').val(),
                    imageUrl:$('.imageUrl').val(),
                    buyerId:refId,
                    number:$('.number').val(),
                    price:$('.price').val(),
                    pack:$('.pack').val(),
                    phone:$('.phone').val(),
                    qq:$('.qq').val(),
                    key:$('.key').val(),
                    description:editor.getContent(),
                    id:''
                })
            }).done(function (res) {
                if(res.errors){
                    return  false
                }
                $('.xinjia').animate({
                    opacity:'0.8'
                }).animate({
                    opacity:'0'
                },2000);
                window.location.href='buyerbox1.html';
            })
        })

    }else{
        Cookies.remove('edit10');
        $('#submit_btn').hide();
        //$('.list').eq(1).hide();
        $.ajax({
            url:host()+'/productBuy/get/'+editId,
        }).done(function (e) {
            var info = e.data;
            $('.name').val(info.name);
            $('.imageUrl').val(info.imageUrl);
            // $('.buyerId').val(info.buyerId);
            $('.number').val(info.number);
            $('.price').val(info.price);
            $('.pack').val(info.pack);
            $('.qq').val(info.qq);
            $('.key').val(info.key);
            //$('.description').val(info.description);
            editor.ready(function () {
                editor.setContent(info.description);
            });
            $('.phone').val(info.phone);
            $('#headPic').attr('src',host()+info.imageUrl);
            $('.quxiao').click(function () {
                window.location.href='buyerbox1.html'
            });
            $('.baocun').click(function () {
                $.ajax({
                    url:host()+'/productBuy/update',
                    type:'put',
                    dataType:'json',
                    contentType:'application/json;charset=utf-8',
                    data:JSON.stringify({
                        name:$('.name').val(),
                        imageUrl:$('.imageUrl').val(),
                        buyerId:$('.buyerId').val(),
                        number:$('.number').val(),
                        price:$('.price').val(),
                        pack:$('.pack').val(),
                        phone:$('.phone').val(),
                        qq:$('.qq').val(),
                        key:$('.key').val(),
                        description:editor.getContent(),
                        id:editId
                    })
                }).done(function (res) {
                    if(res.errors){
                        return  false
                    }
                    $('.xiugai').animate({
                        opacity:'0.8'
                    }).animate({
                        opacity:'0'
                    },2000);
                    window.location.href='buyerbox1.html';
                })
            })
        })
    }


})