$(function () {
    var editId = Cookies.get('edit6');
    if(editId==undefined){
        $('.quxiao').click(function () {
            window.location.href='yonghuguanli.html'
        })
        $('.baocun').click(function () {
            $.ajax({
                url:host()+'/user/add',
                type:'post',
                dataType:'json',
                contentType:'application/json;charset=utf-8',
                data:JSON.stringify({
                    username:$('.username').val(),
                    companyName:$('.companyName').val(),
                    phone:$('.phone').val(),
                    password:$('.password').val(),
                    email:$('.email').val()
                })
            }).done(function () {
                $('.xinjia').animate({
                    opacity:'0.8'
                }).animate({
                    opacity:'0'
                },2000)
            })
        })

    }else{
        Cookies.remove('edit6');
        $.ajax({
            url:host()+'/user/get/'+editId,
        }).done(function (e) {
            var info = e.data;
            $('.companyName').val(info.companyName);
            $('.refId').val(info.refId);
            $('.role').val(info.role);
            $('.phone').val(info.phone);
            $('.password').val(info.password);
            $('.email').val(info.email);

            $('.quxiao').click(function () {
                window.location.href='yonghuguanli.html'
            })
            $('.baocun').click(function () {
                $.ajax({
                    url:host()+'/user/update',
                    type:'put',
                    dataType:'json',
                    contentType:'application/json;charset=utf-8',
                    data:JSON.stringify({
                        companyName:$('.companyName').val(),
                        phone:$('.phone').val(),
                        password:$('.password').val(),
                        email:$('.email').val(),
                        id:editId
                    })
                }).done(function () {
                    $('.xiugai').animate({
                        opacity:'0.8'
                    }).animate({
                        opacity:'0'
                    },2000)
                })
            })
        })
    }


})