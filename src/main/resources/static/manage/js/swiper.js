// var host = host()+'';
$(function () {
    $.ajax({
        url: host() + '/banner/listAll'
    }).done(function (e) {
        console.log(e.data);
        var swiper = e.data;
        var qhtml = '';
        if (!swiper) {
            qhtml += '<div class="error">当前没有滚图！</div>';
            $('.detail').find('tbody').html(qhtml);
            return false;
        }
        for (var i = 0, len = swiper.length; i < len; i++) {
            qhtml += '<tr num="' + i + '">\n' +
                '                            <th class="select" scope="row">\n' +
                '                                <input type="checkbox">\n' +
                '                            </th>\n' +
                '                            <td>' + (swiper[i].sortIndex+1) + '</td>\n' +
                '                            <td>' + swiper[i].name + '</td>\n' +
                '                            <td><img class="imgs" src="' +host()+ swiper[i].imageUrl + '" alt=""/></td>\n' +
                '                            <td>' + swiper[i].linkUrl + '</td>\n' +
                '<td class="caozuo">\n' +
                '                                <a class="top ' + swiper[i].id + '">上移</a>|<a class="bottom ' + swiper[i].id + '">下移</a>\n' +
                '                            </td>\n' +
                '                            <td class="edit">\n' +
                '                                <a></a>\n' +
                '                            </td>\n' +
                '                            <td class="del">\n' +
                '                                <a></a>\n' +
                '                            </td>\n' +
                '                        </tr>';
        }

        $('.detail').find('tbody').html(qhtml);

        //升序
        $('.d-box .caozuo .top').each(function () {
            $(this).click(function (e) {
                var id = e.target.className.split(' ')[1];
                $.ajax({
                    url: host() + '/banner/up/' + id,
                    type: 'PUT',success: function(res){
                        //console.log(res);
                        window.location.reload()
                    }
                })
            })
        });
        //降序
        $('.d-box .caozuo .bottom').each(function () {
            $(this).click(function (e) {
                var id = e.target.className.split(' ')[1];
                $.ajax({
                    url: host() + '/banner/down/' + id,
                    type: 'PUT',
                    success: function(res){
                        //console.log(res);
                        window.location.reload()
                    }
                })
            })
        });

        $('.d-box tbody .select input').each(function () {
            $(this).click(function () {
                console.log($(this).prop('checked'))
                if ($(this).prop('checked') == false) {
                    $('.d-box thead .select input').prop('checked', false);
                    check = $('.d-box thead .select input').prop('checked');
                    $(this).prop('checked', false)
                } else {
                    $(this).prop('checked', true)
                }
            })
        })

        //删除单个
        $('.del a').each(function (d) {
            $(this).click(function () {
                var delId = swiper[d].id;
                var move = '您确认要删除吗？';
                if (confirm(move) == true) {
                    // $(this).parents('tr').remove();
                    $.ajax({
                        url: host() + '/banner/delete/' + delId,
                        type: 'delete'
                    }).done(function () {
                        window.location.reload()
                    })
                } else {
                    return false;
                }
            })
        });

        var checked = [],
            cnum = [];
        $('.m-btn a:eq(1)').click(function () {
            $('tbody .select input:checked').each(function () {
                cnum.push($(this).parent().parent().attr('num'))
                // console.log(cnum)
                for (var i = 0, len = cnum.length; i < len; i++) {
                    checked.push(swiper[cnum[i]].id)
                }
            })
            // console.log(checked)
            var move = '您确认要删除吗？';
            if (confirm(move) == true) {
                for (var i = 0, len = checked.length; i < len; i++) {
                    $.ajax({
                        url: host() + '/banner/delete/' + checked[i],
                        type: "delete"
                    }).done(function () {
                        window.location.reload();
                    })
                }
            } else {
                return false;
            }
        });

        $('.edit a').each(function (e) {
            $(this).click(function () {
                var id = swiper[e].id;
                Cookies.set('swiper', id, 100);
                window.location.href = 'addswiper.html'
            })
        })
    })
})