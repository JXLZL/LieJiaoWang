$(function () {
    var editId = 'liejiao';
    var editor = UE.getEditor('editor');

    //头像预览
    $("#headPic").click(function () {
        $("#image").click(); //隐藏了input:file样式后，点击头像就可以本地上传
        $("#image").on("change", function () {
            var objUrl = getObjectURL(this.files[0]); //获取图片的路径，该路径不是图片在本地的路径
            if (objUrl) {
                $("#headPic").attr("src", objUrl); //将图片路径存入src中，显示出图片
            }
        });
    });
    $("#image").on("change", function () {
        var objUrl = getObjectURL(this.files[0]); //获取图片的路径，该路径不是图片在本地的路径
        if (objUrl) {
            $("#headPic").attr("src", objUrl); //将图片路径存入src中，显示出图片
        }
    });

    //图片上传
    $('#submit_btn').click(function () {
        var formData = new FormData();

        var image = $('input[name=image]')[0].files[0];
        formData.append('image', image);
        formData.append('module', 'company');

        // console.log("这是获取的图片：" + image);
        $.ajax({
            url: host() + '/image/upload',
            type: "POST",
            contentType: false,
            processData: false,
            async: false,
            dataType: "JSON",
            data: formData,
            success: function (result, error, s) {
                if (s) {
                    if (result.data == {}) {

                    } else {
                    }
                } else {
                    alert('上传失败');
                }
            }
        }).done(function (d) {
            $('.imageUrl').val(d.data.url)
        })
    });

    //建立一個可存取到該file的url
    function getObjectURL(file) {
        var url = null;
        if (window.createObjectURL != undefined) { // basic
            url = window.createObjectURL(file);
        } else if (window.URL != undefined) { // mozilla(firefox)
            url = window.URL.createObjectURL(file);
        } else if (window.webkitURL != undefined) { // webkit or chrome
            url = window.webkitURL.createObjectURL(file);
        }
        return url;
    }

    $.ajax({
        url: host() + '/company/get/' + editId,
        success: function(e){
            var info = e.data;
            $('.name').val(info.name);
            $('.address').val(info.area);
            $('.person').val(info.contact);
            $('.email').val(info.email);
            $('.phone').val(info.phone);
            $('.add').val(info.zip);
            //$('.detail').val(info.detail);
            $('.imageUrl').val(info.imageUrl);
            editor.ready(function () {
                editor.setContent(info.detail);
            });
            $('#headPic').attr('src', host() + info.imageUrl);
        }
    });

    $('.baocun').click(function () {
        var detail = editor.getContent();
        console.log(detail);
        $.ajax({
            url: host() + '/company/update',
            type: 'put',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                area: $('.address').val(),
                name: $('.name').val(),
                contact: $('.person').val(),
                email: $('.email').val(),
                phone: $('.phone').val(),
                zip: $('.add').val(),
                detail: detail,
                id: editId,
                intro: '',
                imageUrl: $('.imageUrl').val()
            }),
            success:function(res){
                if(res.errors){
                    alert(res.errors[0].message);
                    return false
                }
                $('.xiugai').animate({
                    opacity: '0.8'
                }).animate({
                    opacity: '0'
                }, 2000);
                //window.location.href='companyIntro.html';
                window.location.reload();
            }
        });
    })
})