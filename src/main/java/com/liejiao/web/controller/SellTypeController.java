package com.liejiao.web.controller;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.dto.SellTypeDTO;
import com.liejiao.web.service.SellTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "供应产品分类")
@RestController
@RequestMapping("/sellType")
public class SellTypeController {
    @Autowired
    private SellTypeService sellTypeService;

    @ApiOperation(value = "新增分类")
    @PostMapping("/add")
    public ActionResult<String> add(@ApiParam("名称") @RequestParam String name){
        return sellTypeService.add(name);
    }

    @ApiOperation(value = "删除分类")
    @DeleteMapping("/delete/{id}")
    public ActionResult<String> delete(@ApiParam("id") @PathVariable String id){
        return sellTypeService.delete(id);
    }

    @ApiOperation(value = "修改分类")
    @PutMapping("/update")
    public ActionResult<String> update(@ApiParam("id") @RequestParam String id,
                                       @ApiParam("名称") @RequestParam String name,
                                       @ApiParam("排序") @RequestParam(required = false) Integer sortIndex){
        return sellTypeService.update(id, name, sortIndex);
    }

    @ApiOperation(value = "获取分类")
    @GetMapping("/get/{id}")
    public ActionResult<SellTypeDTO> get(@ApiParam("id") @PathVariable String id){
        return sellTypeService.get(id);
    }

    @ApiOperation(value = "获取所有分类")
    @GetMapping("/listAll")
    public ActionResult<List<SellTypeDTO>> listAll(){
        return sellTypeService.listAll();
    }

    @ApiOperation(value = "升序")
    @PutMapping("/up/{id}")
    public ActionResult<String> up(@ApiParam("id") @PathVariable String id) {
        return sellTypeService.up(id);
    }

    @ApiOperation(value = "降序")
    @PutMapping("/down/{id}")
    public ActionResult<String> down(@ApiParam("id") @PathVariable String id) {
        return sellTypeService.down(id);
    }

}
