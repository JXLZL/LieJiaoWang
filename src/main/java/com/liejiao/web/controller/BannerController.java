package com.liejiao.web.controller;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.dto.BannerDTO;
import com.liejiao.web.service.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "滚图")
@RestController
@RequestMapping("/banner")
public class BannerController {

    @Autowired
    private BannerService bannerService;

    @ApiOperation(value = "新增滚图")
    @PostMapping("/add")
    public ActionResult<String> add(@ApiParam("名称") @RequestParam String name,
                                    @ApiParam("图片URL") @RequestParam String imageUrl,
                                    @ApiParam("链接") @RequestParam(required = false) String linkUrl) {
        return bannerService.add(name, imageUrl, linkUrl);
    }

    @ApiOperation(value = "删除滚图")
    @DeleteMapping("/delete/{id}")
    public ActionResult<String> delete(@ApiParam("id") @PathVariable String id) {
        return bannerService.delete(id);
    }

    @ApiOperation(value = "更新滚图")
    @PutMapping("/update")
    public ActionResult<String> update(@ApiParam("id") @RequestParam String id,
                                       @ApiParam("名称") @RequestParam String name,
                                       @ApiParam("图片URL") @RequestParam String imageUrl,
                                       @ApiParam("链接") @RequestParam(required = false) String linkUrl,
                                       @ApiParam("排序") @RequestParam(required = false) Integer sortIndex) {
        return bannerService.update(id, name, imageUrl, linkUrl, sortIndex);
    }

    @ApiOperation(value = "获取滚图")
    @GetMapping("/get/{id}")
    public ActionResult<BannerDTO> get(@ApiParam("id") @PathVariable String id) {
        return bannerService.get(id);
    }

    @ApiOperation(value = "查询所有滚图")
    @GetMapping("/listAll")
    public ActionResult<List<BannerDTO>> listAll() {
        return bannerService.listAll();
    }

    @ApiOperation(value = "新增滚图")
    @PutMapping("/up/{id}")
    public ActionResult<String> up(@ApiParam("id") @PathVariable String id) {
        return bannerService.up(id);
    }

    @ApiOperation(value = "新增滚图")
    @PutMapping("/down/{id}")
    public ActionResult<String> down(@ApiParam("id") @PathVariable String id) {
        return bannerService.down(id);
    }
}
