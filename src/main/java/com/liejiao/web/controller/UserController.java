package com.liejiao.web.controller;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.ErrorInfo;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.UserDTO;
import com.liejiao.web.dto.query.UserQueryDTO;
import com.liejiao.web.service.UserService;
import com.liejiao.web.util.PhoneUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "用户")
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiOperation(
            value = "新增用户(管理员)",
            notes = "必填字段：<br/>" +
                    "1.companyName 公司名称<br/>" +
                    "2.email 邮箱<br/>" +
                    "3.phone 手机<br/>" +
                    "4.username 用户名<br/>" +
                    "5.password 密码"
    )
    @PostMapping("/add")
    public ActionResult<String> add(@ApiParam("用户DTO") @RequestBody UserDTO dto){
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getCompanyName()))
            errors.add(new ErrorInfo(0,"公司名称为空"));
        if (StringUtils.isEmpty(dto.getPhone())){
            errors.add(new ErrorInfo(0,"手机为空"));
        }else {
            if (!PhoneUtils.checkPhone(dto.getPhone()))
                errors.add(new ErrorInfo(0,"手机号错误"));
        }
        if (StringUtils.isEmpty(dto.getEmail()))
            errors.add(new ErrorInfo(0,"邮箱为空"));
        if (StringUtils.isEmpty(dto.getUsername()))
            errors.add(new ErrorInfo(0,"用户名为空"));
        if (StringUtils.isEmpty(dto.getPassword()))
            errors.add(new ErrorInfo(0,"密码为空"));

        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return userService.add(dto);
    }

    @ApiOperation(value = "删除用户")
    @DeleteMapping("delete/{id}")
    public ActionResult<String> delete(@ApiParam("id") @PathVariable String id){
        return userService.delete(id);
    }

    @ApiOperation(
            value = "更新用户",
            notes = "必填字段：<br/>" +
                    "1.id 主键<br/>" +
                    "2.email 邮箱<br/>" +
                    "3.phone 手机<br/>"
    )
    @PutMapping("/update")
    public ActionResult<String> update(@ApiParam("用户DTO") @RequestBody UserDTO dto){
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getId()))
            errors.add(new ErrorInfo(0,"id为空"));
        if (StringUtils.isEmpty(dto.getEmail()))
            errors.add(new ErrorInfo(0,"邮箱为空"));
        if (StringUtils.isEmpty(dto.getPhone())){
            errors.add(new ErrorInfo(0,"手机为空"));
        }else {
            if (!PhoneUtils.checkPhone(dto.getPhone()))
                errors.add(new ErrorInfo(0,"手机号错误"));
        }
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return userService.update(dto);
    }

    @ApiOperation(value = "获取用户")
    @GetMapping("/get/{id}")
    public ActionResult<UserDTO> get(@ApiParam("id") @PathVariable String id){
        return userService.get(id);
    }

    @ApiOperation(value = "分页查询用户")
    @GetMapping("/pageQuery")
    public ActionResult<Pagination<UserDTO>> pageQuery(@ApiParam @ModelAttribute UserQueryDTO queryDTO){
        if (queryDTO.getPageNo() == null || queryDTO.getPageNo() <= 0)
            queryDTO.setPageNo(1);
        if (queryDTO.getPageSize() == null || queryDTO.getPageSize() <= 0)
            queryDTO.setPageSize(10);
        return userService.listByQueryDTO(queryDTO);
    }

    @ApiOperation(value = "修改密码", notes = "必填：id，用户名，密码")
    @PutMapping("/resetPassword")
    public ActionResult<String> resetPassword(@ApiParam @RequestBody UserDTO dto){
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getId()))
            errors.add(new ErrorInfo(0,"id为空"));
        if (StringUtils.isEmpty(dto.getUsername()))
            errors.add(new ErrorInfo(0,"用户名为空"));
        if (StringUtils.isEmpty(dto.getPassword()))
            errors.add(new ErrorInfo(0,"密码为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return userService.resetPassword(dto);
    }

}
