package com.liejiao.web.controller;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.ErrorInfo;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.NewsDTO;
import com.liejiao.web.dto.query.NewsQueryDTO;
import com.liejiao.web.service.NewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "新闻")
@RestController
@RequestMapping(value = "/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    @ApiOperation(
            value = "新增新闻",
            notes = "<b>必填属性：</b><br>" +
                    "1.title 标题<br/>"
    )
    @PostMapping(value = "/add")
    public ActionResult<String> add(@ApiParam("新闻DTO") @RequestBody NewsDTO dto) {
        if (StringUtils.isEmpty(dto.getTitle()))
            return new ActionResult<>(0, "标题为空");
        return newsService.add(dto);
    }

    @ApiOperation(value = "删除新闻")
    @DeleteMapping(value = "/delete/{id}")
    public ActionResult<String> delete(@ApiParam("新闻id") @PathVariable String id) {
        return newsService.delete(id);
    }

    @ApiOperation(value = "批量删除新闻")
    @DeleteMapping(value = "/batchDelete")
    public ActionResult<List<String>> batchDelete(@ApiParam("新闻id数组") @RequestParam List<String> ids) {
        if (CollectionUtils.isEmpty(ids))
            return new ActionResult<>(0, "参数不能为空");
        return newsService.batchDelete(ids);
    }

    @ApiOperation(
            value = "修改新闻",
            notes = "<b>必填属性：</b><br>" +
                    "1.id 主键<br/>" +
                    "2.title 标题<br/>"
    )
    @PutMapping(value = "/update")
    public ActionResult<String> update(@ApiParam("新闻DTO") @RequestBody NewsDTO dto) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getId()))
            errors.add(new ErrorInfo(0, "id为空"));
        if (StringUtils.isEmpty(dto.getTitle()))
            errors.add(new ErrorInfo(0, "标题为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return newsService.update(dto);
    }

    @ApiOperation(value = "获取新闻")
    @GetMapping("/get/{id}")
    public ActionResult<NewsDTO> get(@ApiParam("id") @PathVariable String id){
        return newsService.get(id);
    }

    @ApiOperation("分页查询指定条件的新闻")
    @GetMapping("/pageQuery")
    public ActionResult<Pagination<NewsDTO>> pageQuery(@ApiParam("新闻查询DTO") @ModelAttribute NewsQueryDTO queryDTO) {
        //验证页码
        if (queryDTO.getPageNo() == null || queryDTO.getPageNo() <= 0)
            queryDTO.setPageNo(1);
        if (queryDTO.getPageSize() == null || queryDTO.getPageSize() <= 0)
            queryDTO.setPageSize(10);
        return newsService.listByQueryDTO(queryDTO);
    }

    @ApiOperation(value = "增加浏览次数")
    @PutMapping("/visit/{id}")
    public ActionResult<String> visit(@ApiParam("id") @PathVariable String id){
        return newsService.visit(id);
    }
}
