package com.liejiao.web.controller;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.ErrorInfo;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.SellerDTO;
import com.liejiao.web.dto.query.SellerQueryDTO;
import com.liejiao.web.service.SellerService;
import com.liejiao.web.util.PhoneUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "卖家")
@RestController
@RequestMapping("/seller")
public class SellerController {


    @Autowired
    private SellerService sellerService;

    @ApiOperation(
            value = "注册卖家",
            notes = "必填字段：<br/>" +
                    "1.companyName 公司名称<br/>" +
                    "2.contact 联系人<br/>" +
                    "3.phone 手机<br/>" +
                    "4.username 用户名<br/>" +
                    "5.password 密码"
    )
    @PostMapping("/register")
    public ActionResult<String> register(@ApiParam("卖家DTO") @RequestBody SellerDTO dto){
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getCompanyName()))
            errors.add(new ErrorInfo(0,"公司名称为空"));
        if (StringUtils.isEmpty(dto.getContact()))
            errors.add(new ErrorInfo(0,"联系人为空"));
        if (StringUtils.isEmpty(dto.getPhone())){
            errors.add(new ErrorInfo(0,"手机为空"));
        }else {
            if (!PhoneUtils.checkPhone(dto.getPhone()))
                errors.add(new ErrorInfo(0,"手机号错误"));
        }
        if (StringUtils.isEmpty(dto.getEmail()))
            errors.add(new ErrorInfo(0,"邮箱为空"));
        if (StringUtils.isEmpty(dto.getUsername()))
            errors.add(new ErrorInfo(0,"用户名为空"));
        if (StringUtils.isEmpty(dto.getPassword()))
            errors.add(new ErrorInfo(0,"密码为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return sellerService.register(dto);
    }

    @ApiOperation(value = "删除卖家")
    @DeleteMapping("delete/{id}")
    public ActionResult<String> delete(@ApiParam("id") @PathVariable String id){
        return sellerService.delete(id);
    }

    @ApiOperation(
            value = "更新卖家",
            notes = "必填字段：<br/>" +
                    "1.id 主键<br/>" +
                    "2.contact 联系人<br/>" +
                    "3.phone 手机<br/>"
    )
    @PutMapping("/update")
    public ActionResult<String> update(@ApiParam("卖家DTO") @RequestBody SellerDTO dto){
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getId()))
            errors.add(new ErrorInfo(0,"id为空"));
        if (StringUtils.isEmpty(dto.getContact()))
            errors.add(new ErrorInfo(0,"联系人为空"));
        if (StringUtils.isEmpty(dto.getEmail()))
            errors.add(new ErrorInfo(0,"邮箱为空"));
        if (StringUtils.isEmpty(dto.getPhone())){
            errors.add(new ErrorInfo(0,"手机为空"));
        }else {
            if (!PhoneUtils.checkPhone(dto.getPhone()))
                errors.add(new ErrorInfo(0,"手机号错误"));
        }
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return sellerService.update(dto);
    }

    @ApiOperation(value = "获取卖家")
    @GetMapping("/get/{id}")
    public ActionResult<SellerDTO> get(@ApiParam("id") @PathVariable String id){
        return sellerService.get(id);
    }

    @ApiOperation(value = "分页查询卖家")
    @GetMapping("/pageQuery")
    public ActionResult<Pagination<SellerDTO>> pageQuery(@ApiParam @ModelAttribute SellerQueryDTO queryDTO){
        //todo
        if (queryDTO.getPageNo() == null || queryDTO.getPageNo() <= 0)
            queryDTO.setPageNo(1);
        if (queryDTO.getPageSize() == null || queryDTO.getPageSize() <= 0)
            queryDTO.setPageSize(10);
        return sellerService.listByQueryDTO(queryDTO);
    }

    @ApiOperation(value = "审核卖家")
    @PutMapping("/audit/{id}/{pass}")
    public ActionResult<String> audit(@ApiParam("id") @PathVariable("id") String id,
                                      @ApiParam("是否通过") @PathVariable("pass") Boolean pass){
        return sellerService.audit(id, pass);
    }

}
