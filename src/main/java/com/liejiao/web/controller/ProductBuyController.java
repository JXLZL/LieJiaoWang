package com.liejiao.web.controller;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.ErrorInfo;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.ProductBuyDTO;
import com.liejiao.web.dto.query.ProductBuyQueryDTO;
import com.liejiao.web.service.ProductBuyService;
import com.liejiao.web.util.PhoneUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "求购产品")
@RestController
@RequestMapping("/productBuy")
public class ProductBuyController {


    @Autowired
    private ProductBuyService productBuyService;

    @ApiOperation(
            value = "新增求购产品",
            notes = "必填字段：<br/>" +
                    "1.buyerId 买家id<br/>" +
                    "2.name 名称<br/>" +
                    "3.phone 手机<br/>" +
                    "4.key 关键词<br/>"
    )
    @PostMapping("/add")
    public ActionResult<String> add(@ApiParam("求购产品DTO") @RequestBody ProductBuyDTO dto){
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getBuyerId()))
            errors.add(new ErrorInfo(0,"买家id为空"));
        if (StringUtils.isEmpty(dto.getName()))
            errors.add(new ErrorInfo(0,"名称为空"));
        if (StringUtils.isEmpty(dto.getPhone())){
            errors.add(new ErrorInfo(0,"手机为空"));
        }else {
            if (!PhoneUtils.checkPhone(dto.getPhone()))
                errors.add(new ErrorInfo(0,"手机号错误"));
        }
        if (StringUtils.isEmpty(dto.getKey()))
            errors.add(new ErrorInfo(0,"关键词为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return productBuyService.add(dto);
    }

    @ApiOperation(value = "删除求购产品")
    @DeleteMapping("delete/{id}")
    public ActionResult<String> delete(@ApiParam("id") @PathVariable String id){
        return productBuyService.delete(id);
    }

    @ApiOperation(
            value = "更新求购产品",
            notes = "必填字段：<br/>" +
                    "1.id 主键<br/>" +
                    "2.name 名称<br/>" +
                    "3.phone 手机<br/>" +
                    "4.key 关键词<br/>"
    )
    @PutMapping("/update")
    public ActionResult<String> update(@ApiParam("求购产品DTO") @RequestBody ProductBuyDTO dto){
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getId()))
            errors.add(new ErrorInfo(0,"id为空"));
        if (StringUtils.isEmpty(dto.getName()))
            errors.add(new ErrorInfo(0,"名称为空"));
        if (StringUtils.isEmpty(dto.getPhone())){
            errors.add(new ErrorInfo(0,"手机为空"));
        }else {
            if (!PhoneUtils.checkPhone(dto.getPhone()))
                errors.add(new ErrorInfo(0,"手机号错误"));
        }
        if (StringUtils.isEmpty(dto.getKey()))
            errors.add(new ErrorInfo(0,"关键词为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return productBuyService.update(dto);
    }

    @ApiOperation(value = "获取求购产品")
    @GetMapping("/get/{id}")
    public ActionResult<ProductBuyDTO> get(@ApiParam("id") @PathVariable String id){
        return productBuyService.get(id);
    }

    @ApiOperation(value = "分页查询求购产品")
    @GetMapping("/pageQuery")
    public ActionResult<Pagination<ProductBuyDTO>> pageQuery(@ApiParam @ModelAttribute ProductBuyQueryDTO queryDTO){
        //todo
        if (queryDTO.getPageNo() == null || queryDTO.getPageNo() <= 0)
            queryDTO.setPageNo(1);
        if (queryDTO.getPageSize() == null || queryDTO.getPageSize() <= 0)
            queryDTO.setPageSize(10);
        return productBuyService.listByQueryDTO(queryDTO);
    }

    @ApiOperation(value = "增加浏览次数")
    @PutMapping("/visit/{id}")
    public ActionResult<String> visit(@ApiParam("id") @PathVariable String id){
        return productBuyService.visit(id);
    }

    @ApiOperation(value = "模糊检索")
    @GetMapping("/search")
    public ActionResult<Pagination<ProductBuyDTO>> search(@ApiParam("关键词") @RequestParam String key,
                                                           @ApiParam("页码") @RequestParam(required = false) Integer pageNo,
                                                           @ApiParam("大小") @RequestParam(required = false) Integer pageSize) {
        if (pageNo == null || pageNo <= 0)
            pageNo = 1;
        if (pageSize == null || pageSize <= 0)
            pageSize = 10;
        return productBuyService.search(key, pageNo, pageSize);
    }
}
