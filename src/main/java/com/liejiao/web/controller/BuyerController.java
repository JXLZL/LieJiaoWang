package com.liejiao.web.controller;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.ErrorInfo;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.BuyerDTO;
import com.liejiao.web.dto.query.BuyerQueryDTO;
import com.liejiao.web.service.BuyerService;
import com.liejiao.web.util.PhoneUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "买家")
@RestController
@RequestMapping("/buyer")
public class BuyerController {
    @Autowired
    private BuyerService buyerService;

    @ApiOperation(
            value = "注册买家",
            notes = "必填字段：<br/>" +
                    "1.companyName 公司名称<br/>" +
                    "2.contact 联系人<br/>" +
                    "3.phone 手机<br/>" +
                    "4.username 用户名<br/>" +
                    "5.password 密码"
    )
    @PostMapping("/register")
    public ActionResult<String> register(@ApiParam("买家DTO") @RequestBody BuyerDTO dto){
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getCompanyName()))
            errors.add(new ErrorInfo(0,"公司名称为空"));
        if (StringUtils.isEmpty(dto.getContact()))
            errors.add(new ErrorInfo(0,"联系人为空"));
        if (StringUtils.isEmpty(dto.getPhone())){
            errors.add(new ErrorInfo(0,"手机为空"));
        }else {
            if (!PhoneUtils.checkPhone(dto.getPhone()))
                errors.add(new ErrorInfo(0,"手机号错误"));
        }
        if (StringUtils.isEmpty(dto.getEmail()))
            errors.add(new ErrorInfo(0,"邮箱为空"));
        if (StringUtils.isEmpty(dto.getUsername()))
            errors.add(new ErrorInfo(0,"用户名为空"));
        if (StringUtils.isEmpty(dto.getPassword()))
            errors.add(new ErrorInfo(0,"密码为空"));

        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return buyerService.register(dto);
    }

    @ApiOperation(value = "删除买家")
    @DeleteMapping("delete/{id}")
    public ActionResult<String> delete(@ApiParam("id") @PathVariable String id){
        return buyerService.delete(id);
    }

    @ApiOperation(
            value = "更新买家",
            notes = "必填字段：<br/>" +
                    "1.id 主键<br/>" +
                    "2.contact 联系人<br/>" +
                    "3.phone 手机<br/>"
    )
    @PutMapping("/update")
    public ActionResult<String> update(@ApiParam("买家DTO") @RequestBody BuyerDTO dto){
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getId()))
            errors.add(new ErrorInfo(0,"id为空"));
        if (StringUtils.isEmpty(dto.getContact()))
            errors.add(new ErrorInfo(0,"联系人为空"));
        if (StringUtils.isEmpty(dto.getEmail()))
            errors.add(new ErrorInfo(0,"邮箱为空"));
        if (StringUtils.isEmpty(dto.getPhone())){
            errors.add(new ErrorInfo(0,"手机为空"));
        }else {
            if (!PhoneUtils.checkPhone(dto.getPhone()))
                errors.add(new ErrorInfo(0,"手机号错误"));
        }
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return buyerService.update(dto);
    }

    @ApiOperation(value = "获取买家")
    @GetMapping("/get/{id}")
    public ActionResult<BuyerDTO> get(@ApiParam("id") @PathVariable String id){
        return buyerService.get(id);
    }

    @ApiOperation(value = "分页查询买家")
    @GetMapping("/pageQuery")
    public ActionResult<Pagination<BuyerDTO>> pageQuery(@ApiParam @ModelAttribute BuyerQueryDTO queryDTO){
        if (queryDTO.getPageNo() == null || queryDTO.getPageNo() <= 0)
            queryDTO.setPageNo(1);
        if (queryDTO.getPageSize() == null || queryDTO.getPageSize() <= 0)
            queryDTO.setPageSize(10);
        return buyerService.listByQueryDTO(queryDTO);
    }

}
