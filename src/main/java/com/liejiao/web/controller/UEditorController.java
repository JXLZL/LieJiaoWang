package com.liejiao.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

@Api(tags = "ueditor")
@RestController
@RequestMapping("/ueditor")
public class UEditorController {

    @ApiOperation(value = "获取UEditor配置json")
    @GetMapping("/config")
    public String config() throws IOException {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("ueditor-config.json");

//        StringBuilder sb = new StringBuilder();
//        InputStreamReader isr = new InputStreamReader(inputStream,"UTF-8");
//        BufferedReader br = new BufferedReader(isr);
//        String line;
//        while ((line = br.readLine()) != null){
//            sb.append(line);
//        }
        String result = new BufferedReader(new InputStreamReader(inputStream))
                .lines().collect(Collectors.joining(System.lineSeparator()));
        return result;
//        return sb.toString();

//        InputStreamReader isr = new InputStreamReader(inputStream,"UTF-8");


//        URL url = this.getClass().getClassLoader().getResource("ueditor-config.json");
//        File file = new File(url.getFile());
//        FileReader fr = new FileReader(file);
//        BufferedReader br = new BufferedReader(fr);
//        StringBuffer sb = new StringBuffer();
//        String line = null;
//        while ((line = br.readLine()) != null){
//            sb.append(line);
//        }
//        return sb.toString();
    }
}
