package com.liejiao.web.controller;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.CompanyDTO;
import com.liejiao.web.dto.query.CompanyQueryDTO;
import com.liejiao.web.service.CompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "企业")
@RestController
@RequestMapping("/company")
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @ApiOperation(value = "新增公司")
    @PostMapping("/add")
    public ActionResult<String> add(@ApiParam("公司DTO") @RequestBody CompanyDTO dto) {
        return companyService.add(dto);
    }

    @ApiOperation(value = "删除公司")
    @DeleteMapping("/delete/{id}")
    public ActionResult<String> delete(@ApiParam("id") @PathVariable String id) {
        return companyService.delete(id);
    }

    @ApiOperation(value = "批量删除公司")
    @DeleteMapping("/batchDelete")
    public ActionResult<List<String>> batchDelete(@ApiParam("ids") @RequestParam List<String> ids) {
        return companyService.batchDelete(ids);
    }

    @ApiOperation(value = "修改公司")
    @PutMapping("/update")
    public ActionResult<String> update(@ApiParam("公司DTO") @RequestBody CompanyDTO dto) {

        return companyService.update(dto);
    }

    @ApiOperation(value = "获取公司")
    @GetMapping("/get/{id}")
    public ActionResult<CompanyDTO> get(@ApiParam("id") @PathVariable String id) {

        return companyService.get(id);
    }

    @ApiOperation(value = "分页查询公司")
    @GetMapping("/pageQuery")
    public ActionResult<Pagination<CompanyDTO>> pageQuery(@ApiParam("公司查询DTO") @ModelAttribute CompanyQueryDTO queryDTO) {
        //验证页码
        if (queryDTO.getPageNo() == null || queryDTO.getPageNo() <= 0)
            queryDTO.setPageNo(1);
        if (queryDTO.getPageSize() == null || queryDTO.getPageSize() <= 0)
            queryDTO.setPageSize(10);
        return companyService.pageQuery(queryDTO);
    }
}

