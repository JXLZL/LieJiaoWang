package com.liejiao.web.controller;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.dao.UserDao;
import com.liejiao.web.dto.UserDTO;
import com.liejiao.web.entity.User;
import com.liejiao.web.util.EntityUtils;
import com.liejiao.web.util.MD5Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.CredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

@Api(tags = "登录")
@RestController
public class LoginController {
    @Autowired
    private UserDao userDao;

    @ApiOperation(value = "登录跳转")
    @GetMapping("/login")
    public void login(HttpServletResponse response){
        try {
            response.sendRedirect("/login.html");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @ApiOperation("登录接口")
    @PostMapping("/login")
    public ActionResult<UserDTO> login(HttpServletRequest request,
                                       @ApiParam("用户名") @RequestParam String username,
                                       @ApiParam("密码") @RequestParam String password) {
        UsernamePasswordToken token = new UsernamePasswordToken(username, MD5Utils.encryptMD5(password));
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
        } catch (CredentialsException e) {
            e.printStackTrace();
            subject.getSession().removeAttribute("user");
            return new ActionResult<>(-1,"密码错误");
        } catch (Exception e) {
            e.printStackTrace();
            subject.getSession().removeAttribute("user");
        }

        //登陆成功后设置过期时间毫秒
        subject.getSession().setTimeout(1000 * 60 * 60);
        //更新登录时间
        User user = (User) subject.getSession().getAttribute("user");
        user.setLastLoginIp(getIpAddress(request));
        user.setLastLoginTime(new Date());
        userDao.update(user);
        user.setPassword(null);

        return new ActionResult<>(EntityUtils.copyObject(user, UserDTO.class));
    }

    @ApiOperation(value = "登出")
    @GetMapping("/logout")
    public ActionResult<String> logout() {
        SecurityUtils.getSubject().getSession().removeAttribute("user");
        SecurityUtils.getSubject().logout();
        return new ActionResult<>("logout success!");
    }

    private static String getIpAddress(HttpServletRequest request) {

        String ipAddress = request.getHeader("x-forwarded-for");

        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknow".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();

            if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")) {
                //根据网卡获取本机配置的IP地址
                InetAddress inetAddress = null;
                try {
                    inetAddress = InetAddress.getLocalHost();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                ipAddress = inetAddress.getHostAddress();
            }
        }

        //对于通过多个代理的情况，第一个IP为客户端真实的IP地址，多个IP按照','分割
        if (null != ipAddress && ipAddress.length() > 15) {
            //"***.***.***.***".length() = 15
            if (ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }
        }

        return ipAddress;
    }

}
