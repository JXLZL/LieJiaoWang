package com.liejiao.web.controller;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.ErrorInfo;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.ProductSellDTO;
import com.liejiao.web.dto.query.ProductSellQueryDTO;
import com.liejiao.web.service.ProductSellService;
import com.liejiao.web.util.PhoneUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "供应产品")
@RestController
@RequestMapping("/productSell")
public class ProductSellController {


    @Autowired
    private ProductSellService productSellService;

    @ApiOperation(
            value = "新增供应产品",
            notes = "必填字段：<br/>" +
                    "1.sellerId 卖家id<br/>" +
                    "2.name 名称<br/>" +
                    "3.phone 手机<br/>" +
                    "4.key 关键词<br/>"
    )
    @PostMapping("/add")
    public ActionResult<String> add(@ApiParam("供应产品DTO") @RequestBody ProductSellDTO dto) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getSellerId()))
            errors.add(new ErrorInfo(0, "卖家id为空"));
        if (StringUtils.isEmpty(dto.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(dto.getPhone())) {
            errors.add(new ErrorInfo(0, "手机为空"));
        } else {
            if (!PhoneUtils.checkPhone(dto.getPhone()))
                errors.add(new ErrorInfo(0, "手机号错误"));
        }
        if (StringUtils.isEmpty(dto.getKey()))
            errors.add(new ErrorInfo(0, "关键词为空"));
        if (StringUtils.isEmpty(dto.getTypeId()))
            errors.add(new ErrorInfo(0, "分类id为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return productSellService.add(dto);
    }

    @ApiOperation(value = "删除供应产品")
    @DeleteMapping("delete/{id}")
    public ActionResult<String> delete(@ApiParam("id") @PathVariable String id) {
        return productSellService.delete(id);
    }

    @ApiOperation(
            value = "更新供应产品",
            notes = "必填字段：<br/>" +
                    "1.id 主键<br/>" +
                    "2.name 名称<br/>" +
                    "3.phone 手机<br/>" +
                    "4.key 关键词<br/>"
    )
    @PutMapping("/update")
    public ActionResult<String> update(@ApiParam("供应产品DTO") @RequestBody ProductSellDTO dto) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getId()))
            errors.add(new ErrorInfo(0, "id为空"));
        if (StringUtils.isEmpty(dto.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(dto.getPhone())) {
            errors.add(new ErrorInfo(0, "手机为空"));
        } else {
            if (!PhoneUtils.checkPhone(dto.getPhone()))
                errors.add(new ErrorInfo(0, "手机号错误"));
        }
        if (StringUtils.isEmpty(dto.getKey()))
            errors.add(new ErrorInfo(0, "关键词为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return productSellService.update(dto);
    }

    @ApiOperation(value = "获取供应产品")
    @GetMapping("/get/{id}")
    public ActionResult<ProductSellDTO> get(@ApiParam("id") @PathVariable String id) {
        return productSellService.get(id);
    }

    @ApiOperation(value = "分页查询供应产品")
    @GetMapping("/pageQuery")
    public ActionResult<Pagination<ProductSellDTO>> pageQuery(@ApiParam @ModelAttribute ProductSellQueryDTO queryDTO) {
        if (queryDTO.getPageNo() == null || queryDTO.getPageNo() <= 0)
            queryDTO.setPageNo(1);
        if (queryDTO.getPageSize() == null || queryDTO.getPageSize() <= 0)
            queryDTO.setPageSize(10);
        return productSellService.listByQueryDTO(queryDTO);
    }

    @ApiOperation(value = "增加浏览次数")
    @PutMapping("/visit/{id}")
    public ActionResult<String> visit(@ApiParam("id") @PathVariable String id) {
        return productSellService.visit(id);
    }

    @ApiOperation(value = "获取首页展示产品")
    @GetMapping("/homePage")
    public ActionResult<List<ProductSellDTO>> homePage(@ApiParam("数量") @RequestParam Integer size) {
        if (size <= 0)
            size = 10;
        return productSellService.homePage(size);
    }

    @ApiOperation(value = "设置首页展示产品")
    @PutMapping("/setHomePage")
    public ActionResult<String> setHomePage(@ApiParam("id") @RequestParam String id,
                                            @ApiParam("是否首页显示") @RequestParam Boolean homePage,
                                            @ApiParam("首页显示排序") @RequestParam Integer homePageIndex) {
        return productSellService.setHomePage(id, homePage, homePageIndex);
    }

    @ApiOperation(value = "模糊检索")
    @GetMapping("/search")
    public ActionResult<Pagination<ProductSellDTO>> search(@ApiParam("关键词") @RequestParam String key,
                                                           @ApiParam("页码") @RequestParam(required = false) Integer pageNo,
                                                           @ApiParam("大小") @RequestParam(required = false) Integer pageSize) {
        if (pageNo == null || pageNo <= 0)
            pageNo = 1;
        if (pageSize == null || pageSize <= 0)
            pageSize = 10;
        return productSellService.search(key, pageNo, pageSize);
    }

    @ApiOperation(value = "首页产品下标升序")
    @PutMapping("/homePageIndexUp/{id}")
    public ActionResult<String> homePageIndexUp(@ApiParam("id") @PathVariable String id) {
        return productSellService.homePageIndexUp(id);
    }

    @ApiOperation(value = "首页产品下标降序")
    @PutMapping("/homePageIndexDown/{id}")
    public ActionResult<String> homePageIndexDown(@ApiParam("id") @PathVariable String id) {
        return productSellService.homePageIndexDown(id);
    }

    @ApiOperation(value = "刷新首页产品排序")
    @PutMapping("/homePageIndexRefresh")
    public ActionResult<String> homePageIndexRefresh() {
        return productSellService.homePageIndexRefresh();
    }

}
