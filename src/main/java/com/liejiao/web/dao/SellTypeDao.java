package com.liejiao.web.dao;

import com.liejiao.web.entity.SellType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SellTypeDao {
    int insert(SellType sellType);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(SellType sellType);

    SellType get(String id);

    SellType findByName(String name);

    SellType findBySortIndex(Integer sortIndex);

    int batchUpdateSortIndex(@Param("idList") List<String> idList,
                             @Param("updateId") String updateId);

    List<SellType> listAll();

    Integer getMaxSortIndex();
}
