package com.liejiao.web.dao;


import com.liejiao.web.dto.query.ProductSellQueryDTO;
import com.liejiao.web.entity.ProductSell;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface ProductSellDao {
    int insert(ProductSell productSell);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(ProductSell productSell);

    ProductSell get(String id);

    int batchDelete(@Param("idList") List<String> idList, @Param("updateId") String updateId);

    int countByQueryDTO(ProductSellQueryDTO queryDTO);

    List<ProductSell> listByQueryDTO(ProductSellQueryDTO queryDTO);

    int countByNameOrKey(String condition);

    List<ProductSell> findByNameOrKey(@Param("condition") String condition,
                                      @Param("beginIndex") Integer beginIndex,
                                      @Param("pageSize") Integer pageSize);

    int visit(String id);

    List<ProductSell> homePage(Integer size);

    List<ProductSell> listByHomePage();

    int batchUpdateSortIndex(@Param("idList") List<String> idList, @Param("updateId") String updateId);

    int setHomePage(@Param("id") String id,
                    @Param("updateId") String updateId,
                    @Param("homePage") Boolean homePage,
                    @Param("homePageIndex") Integer homePageIndex);

    int countByTypeId(String typeId);

    int batchUpdateHomePageIndex(@Param("idList") List<String> idList, @Param("updateId") String updateId);
}
