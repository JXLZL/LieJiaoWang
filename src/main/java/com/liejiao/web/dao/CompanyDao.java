package com.liejiao.web.dao;

import com.liejiao.web.dto.query.CompanyQueryDTO;
import com.liejiao.web.entity.Company;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CompanyDao {
    int insert(Company company);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int batchDelete(@Param("idList") List<String> idList, @Param("updateId") String updateId);

    int update(Company company);

    Company get(String id);

    int countByQueryDTO(CompanyQueryDTO queryDTO);

    List<Company> listByQueryDTO(CompanyQueryDTO queryDTO);

}
