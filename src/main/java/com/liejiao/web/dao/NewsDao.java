package com.liejiao.web.dao;


import com.liejiao.web.entity.News;
import com.liejiao.web.dto.query.NewsQueryDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewsDao {
    int insert(News news);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(News news);

    News get(String id);

    int batchDelete(@Param("idList") List<String> idList, @Param("updateId") String updateId);

    int countByQueryDTO(NewsQueryDTO queryDTO);

    List<News> listByQueryDTO(NewsQueryDTO queryDTO);

    int visit(String id);

}
