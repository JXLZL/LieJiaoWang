package com.liejiao.web.dao;


import com.liejiao.web.dto.query.ProductBuyQueryDTO;
import com.liejiao.web.entity.ProductBuy;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductBuyDao {
    int insert(ProductBuy productBuy);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(ProductBuy productBuy);

    ProductBuy get(String id);

    int batchDelete(@Param("idList") List<String> idList, @Param("updateId") String updateId);

    int countByQueryDTO(ProductBuyQueryDTO queryDTO);

    List<ProductBuy> listByQueryDTO(ProductBuyQueryDTO queryDTO);

    int countByNameOrKey(String condition);

    List<ProductBuy> findByNameOrKey(@Param("condition") String condition,
                                     @Param("beginIndex") Integer beginIndex,
                                     @Param("pageSize") Integer pageSize);

    int visit(String id);

}
