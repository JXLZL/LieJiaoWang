package com.liejiao.web.dao;

import com.liejiao.web.entity.Banner;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BannerDao {
    int insert(Banner banner);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(Banner banner);

    Banner get(String id);

    Banner findBySortIndex(Integer sortIndex);

    int batchUpdateSortIndex(@Param("idList")List<String> idList,@Param("updateId") String updateId);

    List<Banner> listAll();

    Integer getMaxSortIndex();
}
