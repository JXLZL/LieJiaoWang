package com.liejiao.web.dao;


import com.liejiao.web.dto.query.UserQueryDTO;
import com.liejiao.web.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {
    int insert(User user);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(User user);

    User get(String id);

    int countByQueryDTO(UserQueryDTO queryDTO);

    List<User> listByQueryDTO(UserQueryDTO queryDTO);

    User findByCompanyName(String companyName);

    User findByUsername(String username);

}
