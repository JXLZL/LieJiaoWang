package com.liejiao.web.dao;


import com.liejiao.web.dto.query.SellerQueryDTO;
import com.liejiao.web.entity.Seller;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SellerDao {
    int insert(Seller seller);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(Seller seller);

    Seller get(String id);

    int countByQueryDTO(SellerQueryDTO queryDTO);

    List<Seller> listByQueryDTO(SellerQueryDTO queryDTO);

    Seller findByCompanyName(String companyName);

}
