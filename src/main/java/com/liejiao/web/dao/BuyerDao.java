package com.liejiao.web.dao;


import com.liejiao.web.dto.query.BuyerQueryDTO;
import com.liejiao.web.entity.Buyer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BuyerDao {
    int insert(Buyer buyer);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(Buyer buyer);

    Buyer get(String id);

    int countByQueryDTO(BuyerQueryDTO queryDTO);

    List<Buyer> listByQueryDTO(BuyerQueryDTO queryDTO);

    Buyer findByCompanyName(String companyName);

}
