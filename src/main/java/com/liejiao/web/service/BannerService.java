package com.liejiao.web.service;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.dto.BannerDTO;

import java.util.List;

public interface BannerService {
    ActionResult<String> add(String name, String imageUrl, String linkUrl);

    ActionResult<String> delete(String id);

    ActionResult<String> update(String id, String name, String imageUrl, String linkUrl, Integer sortIndex);

    ActionResult<BannerDTO> get(String id);

    ActionResult<List<BannerDTO>> listAll();

    ActionResult<String> up(String id);

    ActionResult<String> down(String id);
}
