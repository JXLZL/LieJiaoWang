package com.liejiao.web.service;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.SellerDTO;
import com.liejiao.web.dto.query.SellerQueryDTO;

public interface SellerService {
    ActionResult<String> register(SellerDTO sellerDTO);

    ActionResult<String> delete(String id);

    ActionResult<String> update(SellerDTO sellerDTO);

    ActionResult<SellerDTO> get(String id);

    ActionResult<Pagination<SellerDTO>> listByQueryDTO(SellerQueryDTO queryDTO);

    ActionResult<String> audit(String id, Boolean pass);

}
