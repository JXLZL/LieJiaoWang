package com.liejiao.web.service;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.ProductSellDTO;
import com.liejiao.web.dto.query.ProductSellQueryDTO;

import java.util.List;

public interface ProductSellService {
    ActionResult<String> add(ProductSellDTO productSellDTO);

    ActionResult<String> delete(String id);

    ActionResult<List<String>> batchDelete(List<String> ids);

    ActionResult<String> update(ProductSellDTO productSellDTO);

    ActionResult<ProductSellDTO> get(String id);

    ActionResult<Pagination<ProductSellDTO>> listByQueryDTO(ProductSellQueryDTO queryDTO);

    ActionResult<String> visit(String id);

    ActionResult<List<ProductSellDTO>> homePage(Integer size);

    ActionResult<String> setHomePage(String id, Boolean homePage, Integer homePageIndex);

    ActionResult<Pagination<ProductSellDTO>> search(String key, Integer pageNo, Integer pageSize);

    ActionResult<String> homePageIndexUp(String id);

    ActionResult<String> homePageIndexDown(String id);

    ActionResult<String> homePageIndexRefresh();
}
