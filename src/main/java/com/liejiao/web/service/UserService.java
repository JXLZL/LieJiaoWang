package com.liejiao.web.service;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.UserDTO;
import com.liejiao.web.dto.query.UserQueryDTO;

public interface UserService {
    ActionResult<String> add(UserDTO userDTO);

    ActionResult<String> delete(String id);

    ActionResult<String> update(UserDTO userDTO);

    ActionResult<UserDTO> get(String id);

    ActionResult<Pagination<UserDTO>> listByQueryDTO(UserQueryDTO queryDTO);

    ActionResult<String> resetPassword(UserDTO dto);

}
