package com.liejiao.web.service;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.CompanyDTO;
import com.liejiao.web.dto.query.CompanyQueryDTO;

import java.util.List;

public interface CompanyService {
    ActionResult<String> add(CompanyDTO dto);

    ActionResult<String> delete(String id);

    ActionResult<List<String>> batchDelete(List<String> ids);

    ActionResult<String> update(CompanyDTO dto);

    ActionResult<CompanyDTO> get(String id);

    ActionResult<Pagination<CompanyDTO>> pageQuery(CompanyQueryDTO queryDTO);
}
