package com.liejiao.web.service;


import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.NewsDTO;
import com.liejiao.web.dto.query.NewsQueryDTO;

import java.util.List;

public interface NewsService {
    ActionResult<String> add(NewsDTO dto);

    ActionResult<String> delete(String id);

    ActionResult<List<String>> batchDelete(List<String> ids);

    ActionResult<String> update(NewsDTO dto);

    ActionResult<NewsDTO> get(String id);

    ActionResult<Pagination<NewsDTO>> listByQueryDTO(NewsQueryDTO queryDTO);

    ActionResult<String> visit(String id);
}
