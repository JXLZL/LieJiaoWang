package com.liejiao.web.service;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.BuyerDTO;
import com.liejiao.web.dto.query.BuyerQueryDTO;

public interface BuyerService {
    ActionResult<String> register(BuyerDTO buyerDTO);

    ActionResult<String> delete(String id);

    ActionResult<String> update(BuyerDTO buyerDTO);

    ActionResult<BuyerDTO> get(String id);

    ActionResult<Pagination<BuyerDTO>> listByQueryDTO(BuyerQueryDTO queryDTO);

}
