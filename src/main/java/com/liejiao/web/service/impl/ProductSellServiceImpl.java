package com.liejiao.web.service.impl;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.common.SystemContext;
import com.liejiao.web.dao.ProductSellDao;
import com.liejiao.web.dao.SellTypeDao;
import com.liejiao.web.dao.SellerDao;
import com.liejiao.web.dto.ProductSellDTO;
import com.liejiao.web.dto.query.ProductSellQueryDTO;
import com.liejiao.web.entity.ProductSell;
import com.liejiao.web.entity.SellType;
import com.liejiao.web.entity.Seller;
import com.liejiao.web.service.ProductSellService;
import com.liejiao.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductSellServiceImpl implements ProductSellService {
    @Autowired
    private ProductSellDao productSellDao;

    @Autowired
    private SellerDao sellerDao;

    @Autowired
    private SellTypeDao sellTypeDao;

    @Override
    public ActionResult<String> add(ProductSellDTO dto) {
        //1.验证分类是否存在
        SellType type = sellTypeDao.get(dto.getTypeId());
        if (type == null)
            return new ActionResult<>(-1, "新增失败，分类不存在typeId=" + dto.getTypeId());

        // 如果不是猎胶官网则验证卖家
        if (!"liejiao".equals(dto.getSellerId())) {
            // 2.验证卖家是否存在
            Seller seller = sellerDao.get(dto.getSellerId());
            if (seller == null)
                return new ActionResult<>(-1, "新增失败，卖家不存在sellerId=" + dto.getSellerId());
            // 3.验证卖家是否通过审核
            if (!seller.getAudited())
                return new ActionResult<>(-1, "新增失败，卖家未审核无权限新增");
        }


        ProductSell productSell = EntityUtils.copyObjectWithInit(dto, ProductSell.class);
        productSell.setVisits(0);
        productSell.setHomePage(false);
        productSell.setHomePageIndex(0);
        productSellDao.insert(productSell);
        return new ActionResult<>(productSell.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        productSellDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<String>> batchDelete(List<String> ids) {
        productSellDao.batchDelete(ids, SystemContext.getUserId());
        return new ActionResult<>(ids);
    }

    @Override
    public ActionResult<String> update(ProductSellDTO dto) {
        ProductSell po = productSellDao.get(dto.getId());
        if (po == null)
            return new ActionResult<>(-1, "更新失败，不存在id=" + dto.getId() + "的数据");
        po.setUpdateId(SystemContext.getUserId());
        po.setDescription(dto.getDescription());
        po.setContent(dto.getContent());
        po.setImageUrl(dto.getImageUrl());
        po.setKey(dto.getKey());
        po.setPhone(dto.getPhone());
        po.setPrice(dto.getPrice());
        po.setQq(dto.getQq());
        po.setName(dto.getName());
        po.setHomePage(dto.getHomePage());
        po.setHomePageIndex(dto.getHomePageIndex());
        productSellDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<ProductSellDTO> get(String id) {
        ProductSell po = productSellDao.get(id);
        ProductSellDTO dto = EntityUtils.copyObject(po, ProductSellDTO.class);
        return new ActionResult<>(dto);
    }

    @Override
    public ActionResult<Pagination<ProductSellDTO>> listByQueryDTO(ProductSellQueryDTO queryDTO) {
        Pagination<ProductSellDTO> pagination = new Pagination<>(queryDTO.getPageNo(), queryDTO.getPageSize());
        int count = productSellDao.countByQueryDTO(queryDTO);
        pagination.setCount(count);
        if (count > 0) {
            List<ProductSell> poList = productSellDao.listByQueryDTO(queryDTO);
            List<ProductSellDTO> dtoList = EntityUtils.copyList(poList, ProductSellDTO.class);
            pagination.setRows(dtoList);
        }
        return new ActionResult<>(pagination);
    }

    @Override
    public ActionResult<String> visit(String id) {
        productSellDao.visit(id);
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<ProductSellDTO>> homePage(Integer size) {
        return new ActionResult<>(EntityUtils.copyList(productSellDao.homePage(size), ProductSellDTO.class));
    }

    @Override
    public ActionResult<String> setHomePage(String id, Boolean homePage, Integer homePageIndex) {
        ProductSell po = productSellDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "设置失败，数据不存在id=" + id);
        productSellDao.setHomePage(id, SystemContext.getUserId(), homePage, homePageIndex);
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<Pagination<ProductSellDTO>> search(String key, Integer pageNo, Integer pageSize) {
        Pagination<ProductSellDTO> pagination = new Pagination<>(pageNo, pageSize);
        int count = productSellDao.countByNameOrKey(key);
        pagination.setCount(count);
        if (count > 0) {
            List<ProductSell> poList = productSellDao.findByNameOrKey(key, pagination.getBeginIndex(), pageSize);
            List<ProductSellDTO> dtoList = EntityUtils.copyList(poList, ProductSellDTO.class);
            pagination.setRows(dtoList);
        }
        return new ActionResult<>(pagination);
    }

    @Override
    public ActionResult<String> homePageIndexUp(String id) {
        ProductSell po = productSellDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "升序失败，数据不存在id=" + id);
        if (!po.getHomePage())
            return new ActionResult<>(-1, "升序失败，数据非首页显示数据");
        List<ProductSell> poList = productSellDao.listByHomePage();
        if (!CollectionUtils.isEmpty(poList)) {
            for (int i = 0, len = poList.size(); i < len; i++) {
                if (id.equals(poList.get(i).getId()) && i != 0) {
                    poList.set(i, poList.get(i - 1));
                    poList.set(i - 1, po);
                    break;
                }
            }
        }
        List<String> idList = new ArrayList<>();
        for (ProductSell po1 : poList) {
            idList.add(po1.getId());
        }
        productSellDao.batchUpdateHomePageIndex(idList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> homePageIndexDown(String id) {
        ProductSell po = productSellDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "降序失败，数据不存在id=" + id);
        if (!po.getHomePage())
            return new ActionResult<>(-1, "降序失败，数据非首页显示数据");
        List<ProductSell> poList = productSellDao.listByHomePage();
        if (!CollectionUtils.isEmpty(poList)) {
            for (int i = 0, len = poList.size(); i < len; i++) {
                if (id.equals(poList.get(i).getId()) && i != len - 1) {
                    poList.set(i, poList.get(i + 1));
                    poList.set(i + 1, po);
                    break;
                }
            }
        }
        List<String> idList = new ArrayList<>();
        for (ProductSell po1 : poList) {
            idList.add(po1.getId());
        }
        productSellDao.batchUpdateHomePageIndex(idList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> homePageIndexRefresh() {
        List<ProductSell> poList = productSellDao.listByHomePage();
        List<String> idList = new ArrayList<>();
        for (ProductSell po1 : poList) {
            idList.add(po1.getId());
        }
        productSellDao.batchUpdateHomePageIndex(idList, SystemContext.getUserId());
        return new ActionResult<>("刷新成功");
    }
}
