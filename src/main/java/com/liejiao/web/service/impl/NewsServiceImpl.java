package com.liejiao.web.service.impl;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.common.SystemContext;
import com.liejiao.web.dao.NewsDao;
import com.liejiao.web.dto.NewsDTO;
import com.liejiao.web.dto.query.NewsQueryDTO;
import com.liejiao.web.entity.News;
import com.liejiao.web.service.NewsService;
import com.liejiao.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {
    @Autowired
    private NewsDao newsDao;

    @Override
    public ActionResult<String> add(NewsDTO dto) {
        News news = EntityUtils.copyObjectWithInit(dto, News.class);
        news.setVisits(0);
        newsDao.insert(news);
        return new ActionResult<>(news.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        newsDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<String>> batchDelete(List<String> ids) {
        newsDao.batchDelete(ids, SystemContext.getUserId());
        return new ActionResult<>(ids);
    }

    @Override
    public ActionResult<String> update(NewsDTO dto) {
        News po = newsDao.get(dto.getId());
        if (po == null)
            return new ActionResult<>(-1, "修改失败，不存在id=" + dto.getId() + "的数据");
        po.setUpdateId(SystemContext.getUserId());
        po.setTitle(dto.getTitle());
        po.setAuthor(dto.getAuthor());
        po.setSource(dto.getSource());
        po.setIntro(dto.getIntro());
        po.setImageUrl(dto.getImageUrl());
        po.setContent(dto.getContent());
        po.setComment(dto.getComment());
        newsDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<NewsDTO> get(String id) {
        News po = newsDao.get(id);
        NewsDTO dto = EntityUtils.copyObject(po, NewsDTO.class);
        return new ActionResult<>(dto);
    }

    @Override
    public ActionResult<Pagination<NewsDTO>> listByQueryDTO(NewsQueryDTO queryDTO) {
        Pagination<NewsDTO> pagination = new Pagination<>(queryDTO.getPageNo(), queryDTO.getPageSize());
        int count = newsDao.countByQueryDTO(queryDTO);
        pagination.setCount(count);
        if (count > 0) {
            List<News> poList = newsDao.listByQueryDTO(queryDTO);
            List<NewsDTO> dtoList = EntityUtils.copyList(poList, NewsDTO.class);
            pagination.setRows(dtoList);
        }
        return new ActionResult<>(pagination);
    }

    @Override
    public ActionResult<String> visit(String id) {
        newsDao.visit(id);
        return new ActionResult<>(id);
    }
}
