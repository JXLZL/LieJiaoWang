package com.liejiao.web.service.impl;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.SystemContext;
import com.liejiao.web.dao.ProductSellDao;
import com.liejiao.web.dao.SellTypeDao;
import com.liejiao.web.dto.SellTypeDTO;
import com.liejiao.web.entity.SellType;
import com.liejiao.web.service.SellTypeService;
import com.liejiao.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class SellTypeServiceImpl implements SellTypeService {
    @Autowired
    private SellTypeDao sellTypeDao;

    @Autowired
    private ProductSellDao productSellDao;


    @Override
    public ActionResult<String> add(String name) {
        // 验证分类名称是否存在
        SellType po = sellTypeDao.findByName(name);
        if (po != null)
            return new ActionResult<>(-1, "新增分类失败，该名称已存在");
        po = EntityUtils.init(SellType.class);
        po.setName(name);
        Integer max = sellTypeDao.getMaxSortIndex();
        max = max == null ? 0 : max + 1;
        po.setSortIndex(max);
        sellTypeDao.insert(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        SellType po = sellTypeDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "删除失败，数据不存在id=" + id);
        // 验证该分类下是否有数据
        int count = productSellDao.countByTypeId(id);
        if (count > 0)
            return new ActionResult<>(-1,"删除失败，该分类下存在数据，请先删除数据");
        sellTypeDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> update(String id, String name, Integer sortIndex) {
        SellType po = sellTypeDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "更新失败，数据不存在id=" + id);
        Boolean isChanged = false;
        if (!po.getName().equals(name)) {
            SellType po1 = sellTypeDao.findByName(name);
            if (po1 != null)
                return new ActionResult<>(-1, "更新失败，该名称已存在");
            po.setName(name);
            isChanged = true;
        }
        if (sortIndex != null && !po.getSortIndex().equals(sortIndex)) {
            SellType po2 = sellTypeDao.findBySortIndex(sortIndex);
            if (po2 != null)
                return new ActionResult<>(-1, "更新失败，该排序已存在");
            po.setSortIndex(sortIndex);
            isChanged = true;
        }
        if (isChanged)
            sellTypeDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<SellTypeDTO> get(String id) {
        return new ActionResult<>(EntityUtils.copyObject(sellTypeDao.get(id), SellTypeDTO.class));
    }

    @Override
    public ActionResult<List<SellTypeDTO>> listAll() {
        return new ActionResult<>(EntityUtils.copyList(sellTypeDao.listAll(), SellTypeDTO.class));
    }

    @Override
    public ActionResult<String> up(String id) {
        SellType po = sellTypeDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "升序失败，数据不存在id=" + id);
        List<SellType> poList = sellTypeDao.listAll();
        if (!CollectionUtils.isEmpty(poList)) {
            for (int i = 0; i < poList.size(); i++) {
                if (po.getSortIndex().equals(poList.get(i).getSortIndex()) && i != 0) {
                    poList.set(i, poList.get(i - 1));
                    poList.set(i - 1, po);
                    break;
                }
            }
        }
        List<String> idList = new ArrayList<>();
        for (SellType sellType : poList) {
            idList.add(sellType.getId());
        }
        sellTypeDao.batchUpdateSortIndex(idList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> down(String id) {
        SellType po = sellTypeDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "降序失败，数据不存在id=" + id);
        List<SellType> poList = sellTypeDao.listAll();
        if (!CollectionUtils.isEmpty(poList)) {
            for (int i = 0; i < poList.size(); i++) {
                if (po.getSortIndex().equals(poList.get(i).getSortIndex()) && i != poList.size() - 1) {
                    poList.set(i, poList.get(i + 1));
                    poList.set(i + 1, po);
                    break;
                }
            }
        }
        List<String> idList = new ArrayList<>();
        for (SellType sellType : poList) {
            idList.add(sellType.getId());
        }
        sellTypeDao.batchUpdateSortIndex(idList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }
}
