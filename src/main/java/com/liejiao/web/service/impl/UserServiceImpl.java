package com.liejiao.web.service.impl;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.common.SystemContext;
import com.liejiao.web.dao.UserDao;
import com.liejiao.web.dto.UserDTO;
import com.liejiao.web.dto.query.UserQueryDTO;
import com.liejiao.web.entity.User;
import com.liejiao.web.service.UserService;
import com.liejiao.web.util.EntityUtils;
import com.liejiao.web.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public ActionResult<String> add(UserDTO dto) {
        // 1.验证用户名是否存在
        User user = userDao.findByUsername(dto.getUsername());
        if (user != null)
            return new ActionResult<>(-1, "注册失败，用户名已经存在");
        user = EntityUtils.copyObjectWithInit(dto, User.class);

        //保存到用户中心
        user.setRole("管理员");
//        user.setRefId(user.getId());
        user.setPassword(MD5Utils.encryptMD5(dto.getPassword()));
        userDao.insert(user);
        return new ActionResult<>(user.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        userDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }


    @Override
    public ActionResult<String> update(UserDTO dto) {
        User po = userDao.get(dto.getId());
        if (po == null)
            return new ActionResult<>(-1, "更新失败，不存在id=" + dto.getId() + "的数据");
        po.setUpdateId(SystemContext.getUserId());
        po.setEmail(dto.getEmail());
        po.setPhone(dto.getPhone());
        userDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<UserDTO> get(String id) {
        User po = userDao.get(id);
        UserDTO dto = EntityUtils.copyObject(po, UserDTO.class);
        dto.setPassword(null);
        return new ActionResult<>(dto);
    }

    @Override
    public ActionResult<Pagination<UserDTO>> listByQueryDTO(UserQueryDTO queryDTO) {
        Pagination<UserDTO> pagination = new Pagination<>(queryDTO.getPageNo(), queryDTO.getPageSize());
        int count = userDao.countByQueryDTO(queryDTO);
        pagination.setCount(count);
        if (count > 0) {
            List<User> poList = userDao.listByQueryDTO(queryDTO);
            List<UserDTO> dtoList = EntityUtils.copyList(poList, UserDTO.class);
            for (UserDTO dto : dtoList){
                dto.setPassword(null);
            }
            pagination.setRows(dtoList);
        }
        return new ActionResult<>(pagination);
    }

    @Override
    public ActionResult<String> resetPassword(UserDTO dto) {
        //验证用户是否存在
        User user = userDao.get(dto.getId());
        if (user == null)
            return new ActionResult<>(-1,"修改密码失败，用户不存在");
        if (!user.getId().equals(SystemContext.getUserId())){
            if (!"管理员".equals(SystemContext.getRole()))
                return new ActionResult<>(-1,"你无权限对他人的账号进行修改");
        }
        user.setPassword(MD5Utils.encryptMD5(dto.getPassword()));
        user.setUpdateId(SystemContext.getUserId());
        userDao.update(user);
        return new ActionResult<>(user.getId());
    }
}
