package com.liejiao.web.service.impl;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.common.SystemContext;
import com.liejiao.web.dao.CompanyDao;
import com.liejiao.web.dto.CompanyDTO;
import com.liejiao.web.dto.query.CompanyQueryDTO;
import com.liejiao.web.entity.Company;
import com.liejiao.web.service.CompanyService;
import com.liejiao.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyDao companyDao;

    @Override
    public ActionResult<String> add(CompanyDTO dto) {
        Company po = EntityUtils.copyObjectWithInit(dto, Company.class);
        companyDao.insert(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        Company po = companyDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "删除失败，不存在id=" + id + "的数据");
        companyDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<String>> batchDelete(List<String> ids) {
        companyDao.batchDelete(ids, SystemContext.getUserId());
        return new ActionResult<>(ids);
    }

    @Override
    public ActionResult<String> update(CompanyDTO dto) {
        Company po = companyDao.get(dto.getId());
        if (po == null)
            return new ActionResult<>(-1, "更细失败，不存在id=" + dto.getId() + "的数据");
        po.setUpdateId(SystemContext.getUserId());
        po.setName(dto.getName());
        po.setArea(dto.getArea());
        po.setEmail(dto.getEmail());
        po.setZip(dto.getZip());
        po.setContact(dto.getContact());
        po.setDetail(dto.getDetail());
        po.setImageUrl(dto.getImageUrl());
        po.setIntro(dto.getIntro());
        po.setPhone(dto.getPhone());
        po.setContact(dto.getContact());
        companyDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<CompanyDTO> get(String id) {
        Company po = companyDao.get(id);
        return new ActionResult<>(EntityUtils.copyObject(po, CompanyDTO.class));
    }

    @Override
    public ActionResult<Pagination<CompanyDTO>> pageQuery(CompanyQueryDTO queryDTO) {
        Pagination<CompanyDTO> pagination = new Pagination<>(queryDTO.getPageNo(), queryDTO.getPageSize());
        int count = companyDao.countByQueryDTO(queryDTO);
        pagination.setCount(count);
        if (count > 0) {
            List<Company> poList = companyDao.listByQueryDTO(queryDTO);
            pagination.setRows(EntityUtils.copyList(poList, CompanyDTO.class));
        }
        return new ActionResult<>(pagination);
    }
}
