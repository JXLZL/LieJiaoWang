package com.liejiao.web.service.impl;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.common.SystemContext;
import com.liejiao.web.dao.SellerDao;
import com.liejiao.web.dao.UserDao;
import com.liejiao.web.dto.SellerDTO;
import com.liejiao.web.dto.query.SellerQueryDTO;
import com.liejiao.web.entity.Seller;
import com.liejiao.web.entity.User;
import com.liejiao.web.service.SellerService;
import com.liejiao.web.util.EntityUtils;
import com.liejiao.web.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SellerServiceImpl implements SellerService {
    @Autowired
    private SellerDao sellerDao;

    @Autowired
    private UserDao userDao;

    @Override
    public ActionResult<String> register(SellerDTO dto) {

        // 1.验证用户名是否存在
        User user = userDao.findByUsername(dto.getUsername());
        if (user != null)
            return new ActionResult<>(-1, "注册失败，用户名已经存在");

        // 1.验证公司名称是否存在
        Seller seller = sellerDao.findByCompanyName(dto.getCompanyName());
        if (seller == null){
            seller = EntityUtils.copyObjectWithInit(dto, Seller.class);
            sellerDao.insert(seller);
        }

        //保存到用户中心
        user = EntityUtils.init(User.class);
        user.setPhone(dto.getPhone());
        user.setEmail(dto.getEmail());
        user.setCompanyName(dto.getCompanyName());
        user.setRole("卖家");
        user.setRefId(seller.getId());
        user.setUsername(dto.getUsername());
        user.setPassword(MD5Utils.encryptMD5(dto.getPassword()));
        userDao.insert(user);
        return new ActionResult<>(seller.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        sellerDao.delete(id, SystemContext.getUserId());
        //todo 删除该公司所有的供应产品
        /**
         *
         */
        //todo 删除该公司的所有用户
        return new ActionResult<>(id);
    }


    @Override
    public ActionResult<String> update(SellerDTO dto) {
        Seller po = sellerDao.get(dto.getId());
        if (po == null)
            return new ActionResult<>(-1, "更新失败，不存在id=" + dto.getId() + "的数据");
        po.setUpdateId(SystemContext.getUserId());
        po.setArea(dto.getArea());
        po.setContact(dto.getContact());
        po.setEmail(dto.getEmail());
        po.setPhone(dto.getPhone());
        sellerDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<SellerDTO> get(String id) {
        Seller po = sellerDao.get(id);
        SellerDTO dto = EntityUtils.copyObject(po, SellerDTO.class);
        return new ActionResult<>(dto);
    }

    @Override
    public ActionResult<Pagination<SellerDTO>> listByQueryDTO(SellerQueryDTO queryDTO) {
        Pagination<SellerDTO> pagination = new Pagination<>(queryDTO.getPageNo(), queryDTO.getPageSize());
        int count = sellerDao.countByQueryDTO(queryDTO);
        pagination.setCount(count);
        if (count > 0) {
            List<Seller> poList = sellerDao.listByQueryDTO(queryDTO);
            List<SellerDTO> dtoList = EntityUtils.copyList(poList, SellerDTO.class);
            pagination.setRows(dtoList);
        }
        return new ActionResult<>(pagination);
    }

    @Override
    public ActionResult<String> audit(String id, Boolean pass) {
        Seller po = sellerDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "审核失败，数据不存在");
        po.setAudited(pass);
        po.setUpdateId(SystemContext.getUserId());
        sellerDao.update(po);
        return new ActionResult<>(id);
    }

}
