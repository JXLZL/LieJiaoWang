package com.liejiao.web.service.impl;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.SystemContext;
import com.liejiao.web.dao.BannerDao;
import com.liejiao.web.dto.BannerDTO;
import com.liejiao.web.entity.Banner;
import com.liejiao.web.service.BannerService;
import com.liejiao.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    private BannerDao bannerDao;

    @Override
    public ActionResult<String> add(String name, String imageUrl, String linkUrl) {
        Banner po = EntityUtils.init(Banner.class);
        po.setName(name);
        po.setImageUrl(imageUrl);
        po.setLinkUrl(linkUrl);
        Integer max = bannerDao.getMaxSortIndex();
        max = max == null ? 0 : max + 1;
        po.setSortIndex(max);
        bannerDao.insert(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        bannerDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> update(String id, String name, String imageUrl, String linkUrl, Integer sortIndex) {
        Banner po = bannerDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "更新失败，数据不存在id=" + id);
        po.setName(name);
        po.setImageUrl(imageUrl);
        po.setLinkUrl(linkUrl);
        if (sortIndex != null && !sortIndex.equals(po.getSortIndex())) {
            Banner banner = bannerDao.findBySortIndex(sortIndex);
            if (banner != null)
                return new ActionResult<>(-1, "更新失败，排序已经存在");
            po.setSortIndex(sortIndex);
        }
        po.setUpdateId(SystemContext.getUserId());
        bannerDao.update(po);
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<BannerDTO> get(String id) {
        return new ActionResult<>(EntityUtils.copyObject(bannerDao.get(id), BannerDTO.class));
    }

    @Override
    public ActionResult<List<BannerDTO>> listAll() {
        return new ActionResult<>(EntityUtils.copyList(bannerDao.listAll(), BannerDTO.class));
    }

    @Override
    public ActionResult<String> up(String id) {
        Banner po = bannerDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "升序失败，数据不存在id=" + id);
        List<Banner> poList = bannerDao.listAll();
        if (!CollectionUtils.isEmpty(poList)) {
            for (int i = 0, size = poList.size(); i < size; i++) {
                if (po.getSortIndex().equals(poList.get(i).getSortIndex()) && i != 0) {
                    poList.set(i, poList.get(i - 1));
                    poList.set(i - 1, po);
                    break;
                }
            }
        }
        List<String> idList = new ArrayList<>();
        for (Banner banner : poList) {
            idList.add(banner.getId());
        }
        bannerDao.batchUpdateSortIndex(idList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> down(String id) {
        Banner po = bannerDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "降序失败，数据不存在id=" + id);
        List<Banner> poList = bannerDao.listAll();
        if (!CollectionUtils.isEmpty(poList)) {
            for (int i = 0, size = poList.size(); i < size; i++) {
                if (po.getSortIndex().equals(poList.get(i).getSortIndex()) && i != size - 1) {
                    poList.set(i, poList.get(i + 1));
                    poList.set(i + 1, po);
                    break;
                }
            }
        }
        List<String> idList = new ArrayList<>();
        for (Banner banner : poList) {
            idList.add(banner.getId());
        }
        bannerDao.batchUpdateSortIndex(idList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }
}
