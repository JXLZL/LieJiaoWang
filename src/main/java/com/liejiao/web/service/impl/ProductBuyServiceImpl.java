package com.liejiao.web.service.impl;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.common.SystemContext;
import com.liejiao.web.dao.ProductBuyDao;
import com.liejiao.web.dao.BuyerDao;
import com.liejiao.web.dto.ProductBuyDTO;
import com.liejiao.web.dto.query.ProductBuyQueryDTO;
import com.liejiao.web.entity.ProductBuy;
import com.liejiao.web.entity.Buyer;
import com.liejiao.web.service.ProductBuyService;
import com.liejiao.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductBuyServiceImpl implements ProductBuyService {
    @Autowired
    private ProductBuyDao productBuyDao;

    @Autowired
    private BuyerDao buyerDao;

    @Override
    public ActionResult<String> add(ProductBuyDTO dto) {
        // 1.验证买家是否存在
        Buyer buyer = buyerDao.get(dto.getBuyerId());
        if (buyer == null)
            return new ActionResult<>(-1,"新增失败，买家不存在buyerId="+dto.getBuyerId());

        ProductBuy productBuy = EntityUtils.copyObjectWithInit(dto, ProductBuy.class);
        productBuy.setVisits(0);
        productBuyDao.insert(productBuy);
        return new ActionResult<>(productBuy.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        productBuyDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<String>> batchDelete(List<String> ids) {
        productBuyDao.batchDelete(ids, SystemContext.getUserId());
        return new ActionResult<>(ids);
    }

    @Override
    public ActionResult<String> update(ProductBuyDTO dto) {
        ProductBuy po = productBuyDao.get(dto.getId());
        if (po == null)
            return new ActionResult<>(-1, "更新失败，不存在id=" + dto.getId() + "的数据");
        po.setUpdateId(SystemContext.getUserId());
        po.setDescription(dto.getDescription());
        po.setContent(dto.getContent());
        po.setImageUrl(dto.getImageUrl());
        po.setKey(dto.getKey());
        po.setPhone(dto.getPhone());
        po.setPrice(dto.getPrice());
        po.setQq(dto.getQq());
        po.setName(dto.getName());
        productBuyDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<ProductBuyDTO> get(String id) {
        ProductBuy po = productBuyDao.get(id);
        ProductBuyDTO dto = EntityUtils.copyObject(po, ProductBuyDTO.class);
        return new ActionResult<>(dto);
    }

    @Override
    public ActionResult<Pagination<ProductBuyDTO>> listByQueryDTO(ProductBuyQueryDTO queryDTO) {
        Pagination<ProductBuyDTO> pagination = new Pagination<>(queryDTO.getPageNo(), queryDTO.getPageSize());
        int count = productBuyDao.countByQueryDTO(queryDTO);
        pagination.setCount(count);
        if (count > 0) {
            List<ProductBuy> poList = productBuyDao.listByQueryDTO(queryDTO);
            List<ProductBuyDTO> dtoList = EntityUtils.copyList(poList, ProductBuyDTO.class);
            pagination.setRows(dtoList);
        }
        return new ActionResult<>(pagination);
    }

    @Override
    public ActionResult<String> visit(String id) {
        productBuyDao.visit(id);
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<Pagination<ProductBuyDTO>> search(String key, Integer pageNo, Integer pageSize) {
        Pagination<ProductBuyDTO> pagination = new Pagination<>(pageNo, pageSize);
        int count = productBuyDao.countByNameOrKey(key);
        pagination.setCount(count);
        if (count > 0) {
            List<ProductBuy> poList = productBuyDao.findByNameOrKey(key, pagination.getBeginIndex(), pageSize);
            List<ProductBuyDTO> dtoList = EntityUtils.copyList(poList, ProductBuyDTO.class);
            pagination.setRows(dtoList);
        }
        return new ActionResult<>(pagination);
    }

}
