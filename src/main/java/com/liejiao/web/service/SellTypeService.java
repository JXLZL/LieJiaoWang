package com.liejiao.web.service;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.dto.SellTypeDTO;

import java.util.List;

public interface SellTypeService {

    ActionResult<String> add(String name);

    ActionResult<String> delete(String id);

    ActionResult<String> update(String id, String name, Integer sortIndex);

    ActionResult<SellTypeDTO> get(String id);

    ActionResult<List<SellTypeDTO>> listAll();

    ActionResult<String> up(String id);

    ActionResult<String> down(String id);
}
