package com.liejiao.web.service;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.common.Pagination;
import com.liejiao.web.dto.ProductBuyDTO;
import com.liejiao.web.dto.query.ProductBuyQueryDTO;

import java.util.List;

public interface ProductBuyService {
    ActionResult<String> add(ProductBuyDTO productBuyDTO);

    ActionResult<String> delete(String id);

    ActionResult<List<String>> batchDelete(List<String> ids);

    ActionResult<String> update(ProductBuyDTO productBuyDTO);

    ActionResult<ProductBuyDTO> get(String id);

    ActionResult<Pagination<ProductBuyDTO>> listByQueryDTO(ProductBuyQueryDTO queryDTO);

    ActionResult<String> visit(String id);

    ActionResult<Pagination<ProductBuyDTO>> search(String key, Integer pageNo, Integer pageSize);
}
