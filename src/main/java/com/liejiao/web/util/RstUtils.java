package com.liejiao.web.util;

import com.liejiao.web.common.ActionResult;
import com.liejiao.web.enums.Operation;

//返回工具类
public class RstUtils {
    public static <T> ActionResult<T> ok(T t) {
        return new ActionResult<>(t);
    }

    public static ActionResult noData(String id, Operation operation) {
        return new ActionResult(-1, operation.getMessage() + "，数据不存在，id=" + id);
    }

    public static ActionResult nullError(Object obj, String id, Operation operation) {
        return noData(id, operation);
    }

}

