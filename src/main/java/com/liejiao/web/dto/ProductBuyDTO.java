package com.liejiao.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("求购产品DTO")
public class ProductBuyDTO extends BaseDTO {
    @ApiModelProperty("买家id")
    private String buyerId;

    @ApiModelProperty("产品名称")
    private String name;

    @ApiModelProperty("产品关键词")
    private String key;

    @ApiModelProperty("产品描述")
    private String description;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("价格要求")
    private String price;

    @ApiModelProperty("包装要求")
    private String pack;

    @ApiModelProperty("需求数量")
    private String number;

    @ApiModelProperty("联系电话")
    private String phone;

    @ApiModelProperty("联系QQ")
    private String qq;

    @ApiModelProperty("产品图片URL")
    private String imageUrl;

    @ApiModelProperty("浏览次数")
    private Integer visits;
}
