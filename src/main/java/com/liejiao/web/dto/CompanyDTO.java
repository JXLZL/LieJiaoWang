package com.liejiao.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("公司（企业）DTO")
public class CompanyDTO extends BaseDTO {
    @ApiModelProperty("公司名称")
    private String name;

    @ApiModelProperty("地区")
    private String area;

    @ApiModelProperty("联系人")
    private String contact;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("邮政编码")
    private String zip;

    @ApiModelProperty("电话")
    private String phone;

    @ApiModelProperty("简介")
    private String intro;

    @ApiModelProperty("详细资料")
    private String detail;

    @ApiModelProperty("图片")
    private String imageUrl;

}
