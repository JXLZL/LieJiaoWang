package com.liejiao.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("供应产品")
public class ProductSellDTO extends BaseDTO {
    @ApiModelProperty("分类id")
    private String typeId;

    @ApiModelProperty("卖家id")
    private String sellerId;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("关键词")
    private String key;

    @ApiModelProperty("描述")
    private String description;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("价格")
    private String price;

    @ApiModelProperty("手机")
    private String phone;

    @ApiModelProperty("联系QQ")
    private String qq;

    @ApiModelProperty("图片URL")
    private String imageUrl;

    @ApiModelProperty("浏览次数")
    private Integer visits;

    @ApiModelProperty("首页是否显示")
    private Boolean homePage;

    @ApiModelProperty("首页显示下标")
    private Integer homePageIndex;
}
