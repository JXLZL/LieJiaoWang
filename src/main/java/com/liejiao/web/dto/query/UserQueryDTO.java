package com.liejiao.web.dto.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("用户查询DTO")
public class UserQueryDTO extends BaseQueryDTO {

    @ApiModelProperty("角色：企业管理员 卖家 买家")
    private String role;
}
