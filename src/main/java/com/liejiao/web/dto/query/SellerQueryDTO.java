package com.liejiao.web.dto.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("卖家查询DTO")
public class SellerQueryDTO extends BaseQueryDTO {
    @ApiModelProperty("公司名称")
    private String companyName;

    @ApiModelProperty("联系人")
    private String contact;

    @ApiModelProperty("地区")
    private String area;

    @ApiModelProperty("是否已审核")
    private Boolean audited;
}
