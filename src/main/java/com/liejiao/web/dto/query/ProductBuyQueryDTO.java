package com.liejiao.web.dto.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("求购产品查询DTO")
public class ProductBuyQueryDTO extends BaseQueryDTO {
    @ApiModelProperty("买家id")
    private String buyerId;

    @ApiModelProperty("产品名称")
    private String name;

    @ApiModelProperty("产品关键词")
    private String key;
}
