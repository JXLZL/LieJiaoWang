package com.liejiao.web.dto.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("供应产品查询DTO")
public class ProductSellQueryDTO extends BaseQueryDTO {
    @ApiModelProperty("分类id")
    private String typeId;

    @ApiModelProperty("卖家id")
    private String sellerId;

    @ApiModelProperty("产品名称")
    private String name;

    @ApiModelProperty("产品关键词")
    private String key;

}
