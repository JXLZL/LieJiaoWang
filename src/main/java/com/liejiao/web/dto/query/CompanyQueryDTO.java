package com.liejiao.web.dto.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("CompanyQueryDTO")
@ApiModel("公司查询DTO")
public class CompanyQueryDTO extends BaseQueryDTO {

    @ApiModelProperty("公司名称")
    private String name;

    @ApiModelProperty("地区")
    private String area;

    @ApiModelProperty("联系人")
    private String contact;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("邮政编码")
    private String zip;

    @ApiModelProperty("电话")
    private String phone;

    @ApiModelProperty("简介")
    private String intro;

    @ApiModelProperty("详细资料")
    private String detail;

}
