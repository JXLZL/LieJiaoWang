package com.liejiao.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("供应产品分类DTO")
public class SellTypeDTO extends BaseDTO {
    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("排序")
    private Integer sortIndex;
}
