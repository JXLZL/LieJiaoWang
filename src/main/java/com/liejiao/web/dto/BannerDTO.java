package com.liejiao.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("滚动图")
public class BannerDTO extends BaseDTO {

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("图片url")
    private String imageUrl;

    @ApiModelProperty("链接url")
    private String linkUrl;

    @ApiModelProperty("排序")
    private Integer sortIndex;
}
