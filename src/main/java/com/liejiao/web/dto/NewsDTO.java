package com.liejiao.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "新闻")
public class NewsDTO extends BaseDTO {

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("作者")
    private String author;

    @ApiModelProperty("来源")
    private String source;

    @ApiModelProperty("简介")
    private String intro;

    @ApiModelProperty("图片url")
    private String imageUrl;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("备注")
    private String comment;

    @ApiModelProperty("浏览次数")
    private Integer visits;

}
