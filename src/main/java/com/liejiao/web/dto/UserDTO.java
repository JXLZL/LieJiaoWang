package com.liejiao.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("用户DTO")
public class UserDTO extends BaseDTO {
    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("手机")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("公司名称")
    private String companyName;

    @ApiModelProperty("关联公司id")
    private String refId;

    @ApiModelProperty("角色：企业管理员 卖家 买家")
    private String role;

    @ApiModelProperty("最后一次登录IP")
    private String lastLoginIp;

    @ApiModelProperty("最后一次登录时间")
    private Date lastLoginTime;
}
