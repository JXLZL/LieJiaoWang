package com.liejiao.web.shiro;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    @Autowired
    private AnyRolesAuthorizationFilter anyRolesAuthorizationFilter;

    @Bean
    public ShiroRealm shiroRealm() {
        return new ShiroRealm();
    }

    @Bean
    public SecurityManager securityManager(ShiroRealm shiroRealm) {
        return new DefaultWebSecurityManager(shiroRealm);
    }

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();

        /*添加自定义拦截器*/
        Map<String, Filter> filters = shiroFilterFactoryBean.getFilters();
        filters.put("anyRoles", anyRolesAuthorizationFilter);
        shiroFilterFactoryBean.setFilters(filters);

        shiroFilterFactoryBean.setSecurityManager(securityManager);
        shiroFilterFactoryBean.setLoginUrl("/login");
        shiroFilterFactoryBean.setSuccessUrl("/index");
        shiroFilterFactoryBean.setUnauthorizedUrl("/403");
        Map<String, String> mapFilters = new LinkedHashMap<>();

        //静态资源
        mapFilters.put("/css/**", "anon");
        mapFilters.put("/js/**", "anon");
        mapFilters.put("/img/**", "anon");
        mapFilters.put("/upload/**", "anon");
        mapFilters.put("/logout", "anon");
        mapFilters.put("/ueditor/**","anon");

        //用户接口
        mapFilters.put("/user/resetPassword", "authc");
        mapFilters.put("/user/**", "roles[管理员]");

        //查询接口路径
        mapFilters.put("/*/get/**", "anon");
        mapFilters.put("/*/pageQuery**", "anon");



        //首页滚图接口
        mapFilters.put("/banner/listAll","anon");
        mapFilters.put("/banner/**", "roles[管理员]");
        //新闻接口
        mapFilters.put("/news/visit/**","anon");
        mapFilters.put("/news/**", "roles[管理员]");

        //图片接口
        mapFilters.put("/image/**", "authc");
        //企业接口
        mapFilters.put("/company/**", "roles[管理员]");
        //买家接口
        mapFilters.put("/buyer/register", "anon");
        mapFilters.put("/buyer/delete/**", "roles[管理员]");
        mapFilters.put("/buyer/update", "anyRoles[管理员,买家]");
        //卖家接口
        mapFilters.put("/seller/register", "anon");
        mapFilters.put("/seller/delete/**", "roles[管理员]");
        mapFilters.put("/seller/update", "anyRoles[管理员,卖家]");
        //求购产品接口
        mapFilters.put("/productBuy/add", "anyRoles[管理员,买家]");
        mapFilters.put("/productBuy/delete/**", "anyRoles[管理员,买家]");
        mapFilters.put("/productBuy/update", "anyRoles[管理员,买家]");
        mapFilters.put("/productBuy/visit/**", "anon");
        mapFilters.put("/productBuy/search**","anon");
        //供应产品接口
        mapFilters.put("/productSell/add", "anyRoles[管理员,卖家]");
        mapFilters.put("/productSell/delete/**", "anyRoles[管理员,卖家]");
        mapFilters.put("/productSell/update", "anyRoles[管理员,卖家]");
        mapFilters.put("/productSell/setHomePage/**", "roles[管理员]");
        mapFilters.put("/productSell/up/**", "roles[管理员]");
        mapFilters.put("/productSell/down/**", "roles[管理员]");
        mapFilters.put("/productSell/visit/**", "anon");
        mapFilters.put("/productSell/search**","anon");
        //供应产品分类接口
        mapFilters.put("/sellType/listAll","anon");
        mapFilters.put("/sellType/**", "roles[管理员]");

        //ueditor
        mapFilters.put("/ueditor**", "anon");

        //后台管理界面
        mapFilters.put("/manage/**", "authc");


        shiroFilterFactoryBean.setFilterChainDefinitionMap(mapFilters);
        System.out.println("注入权限...");
        return shiroFilterFactoryBean;
    }

    @Bean
    public FilterRegistrationBean delegatingFilterProxy() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        DelegatingFilterProxy proxy = new DelegatingFilterProxy();
        proxy.setTargetFilterLifecycle(true);
        proxy.setTargetBeanName("shiroFilterFactoryBean");
        filterRegistrationBean.setFilter(proxy);
        return filterRegistrationBean;
    }
}
