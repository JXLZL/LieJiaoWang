package com.liejiao.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

//买家
@Data
@Alias("Buyer")
public class Buyer extends BaseEntity {
    private String companyName;             //公司名称

    private String contact;                 //联系人

    private String email;                   //邮箱

    private String phone;                   //手机

    private String area;                    //地区
}
