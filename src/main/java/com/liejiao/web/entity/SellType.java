package com.liejiao.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

//供应产品分类
@Data
@Alias("SellType")
public class SellType extends BaseEntity {
    private String name;                //名称

    private Integer sortIndex;          //排序
}
