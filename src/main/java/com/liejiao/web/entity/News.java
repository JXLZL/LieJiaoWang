package com.liejiao.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("News")
public class News extends BaseEntity {

    private String title;               //标题

    private String author;              //作者

    private String source;              //来源

    private String intro;               //简介

    private String imageUrl;            //图片url

    private String content;             //内容

    private String comment;             //备注

    private Integer visits;             //浏览次数

}
