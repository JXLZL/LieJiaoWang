package com.liejiao.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("Banner")
public class Banner extends BaseEntity {
    private String name;            //名称

    private String imageUrl;        //图片url

    private String linkUrl;         //链接url

    private Integer sortIndex;      //
}
