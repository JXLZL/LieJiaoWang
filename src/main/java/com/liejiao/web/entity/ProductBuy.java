package com.liejiao.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

//求购产品
@Data
@Alias("ProductBuy")
public class ProductBuy extends BaseEntity {
    private String buyerId;             //买家id

    private String name;                //产品名称

    private String key;                 //产品关键词

    private String description;         //产品描述

    private String content;             //内容

    private String price;               //价格要求

    private String pack;                //包装要求

    private String number;              //需求数量

    private String phone;               //联系手机

    private String qq;                  //联系QQ

    private String imageUrl;            //产品图片URL

    private Integer visits;             //浏览次数
}
