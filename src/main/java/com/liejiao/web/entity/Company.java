package com.liejiao.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("Company")
public class Company extends BaseEntity {
    private String name;            //公司名称

    private String area;            //地区

    private String contact;         //联系人

    private String email;           //邮箱

    private String zip;             //邮政编码

    private String phone;           //电话

    private String intro;           //简介

    private String detail;          //详细资料

    private String imageUrl;        //图片url
}
