package com.liejiao.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

//供应产品
@Data
@Alias("ProductSell")
public class ProductSell extends BaseEntity {
    private String typeId;              //分类id

    private String sellerId;            //卖家id

    private String name;                //名称

    private String key;                 //关键词

    private String description;         //描述

    private String content;             //内容

    private String price;               //价格

    private String phone;               //手机

    private String qq;                  //联系QQ

    private String imageUrl;            //图片URL

    private Integer visits;             //浏览次数

    private Boolean homePage;           //首页是否显示

    private Integer homePageIndex;      //首页显示下标

}
