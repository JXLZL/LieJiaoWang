package com.liejiao.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

//卖家
@Data
@Alias("Seller")
public class Seller extends BaseEntity {
    private String companyName;             //公司名称

    private String contact;                 //联系人

    private String email;                   //邮箱

    private String phone;                   //手机

    private String area;                    //地区

    private Boolean audited;                //是否已审核
}
