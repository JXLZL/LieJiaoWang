package com.liejiao.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.Date;

@Data
@Alias("User")
public class User extends BaseEntity {
    private String username;            //用户名

    private String password;            //密码

    private String phone;               //手机

    private String email;               //邮箱

    private String companyName;         //公司名称

    private String refId;               //关联公司id

    private String role;                //角色：企业管理员 卖家 买家

    private String lastLoginIp;         //最后一次登录IP

    private Date lastLoginTime;         //最后一次登录时间
}
