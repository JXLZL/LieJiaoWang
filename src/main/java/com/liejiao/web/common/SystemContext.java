package com.liejiao.web.common;

import com.liejiao.web.entity.User;
import org.apache.shiro.SecurityUtils;

public class SystemContext {
    public static String getUserId(){
        String userId = "system";
        try {
            User user = (User) SecurityUtils.getSubject().getSession().getAttribute("user");
            userId = user.getId();
        }catch (Exception e){
            e.printStackTrace();
        }
        return userId;
    }

    public static String getRole(){
        String role = "system";
        try{
            User user = (User) SecurityUtils.getSubject().getSession().getAttribute("user");
            role = user.getRole();
        }catch (Exception e){
            e.printStackTrace();
        }
        return role;
    }
}
